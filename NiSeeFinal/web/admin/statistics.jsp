<%-- 
    Document   : statistics
    Created on : 02/10/2010, 3:29:10 PM
    Author     : stussy
--%>

<%@page import="niseeEntity.NiseeColourComboSet"%>
<%@page import="niseeEntity.NiseeTestShape"%>
<%@page import="niseeEntity.NiseeTestType"%>
<%@page import="niseeEntity.NiseeUser"%>
<%@page import="niseeDatabase.niseeDB"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="scripts/jscharts.js"></script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Statistics</title>
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
        <%
                    niseeDB db = new niseeDB();
        %>
        <script type="text/javascript">
            <%
                        short maxShowUsage = 5;//maximum number of users to show who have images
                        short maxShowFail = 5;//maximum number of users to show who have failed tests
                        List<niseeDB.Usage> myUsages = db.getImageUsageStats(maxShowUsage);
                        int usageSize = myUsages.size();
                        List<niseeDB.Usage> myFailStats = db.getTestFailStats(maxShowFail);
                        int failStatsSize = myFailStats.size();
                        int totalFailed = 0;
            %>
                var myData = new Array();
                var myData2 = new Array();
            <%
                        for (int i = 0; i < usageSize; i++) {%>
                            myData[<%= i%>] = ['<%=myUsages.get(i).getUsername()%>',<%=myUsages.get(i).getUsage()%>];
            <% }
                        for (int i = 0; i < failStatsSize; i++) {%>
                            myData2[<%= i%>] = ['<%=myFailStats.get(i).getUsername()%>',<%=myFailStats.get(i).getUsage()%>];
            <%totalFailed += myFailStats.get(i).getUsage();%>
            <% }
            %>

        </script>
    </head>
    <body>
        <form action="Statistics" method="POST" id="statisticsForm">
            <div id="wrap">
                <div id="content">
                    <div id="title">
                        <h1>Reports</h1>
                    </div>
                               <div id="side">
                <%@ include file="side.jsp" %>
            </div>
                    <div>
                        <h2>Summary</h2>
                    </div>
            <div>
                        <div style="width: 50%; float: left">
                            <table id="testType" width="100%">
                                <tbody>
                                    <tr class="odd">
                                        <td>Total number of tests</td>
                                        <td><%=db.getNiseeTestTypes().size()%></td>
                                    </tr>
                                    <tr class="even">
                                        <td>Total number of users</td>
                                        <td><%=db.getUsers().size()%></td>
                                    </tr>
                                    <tr class="odd">
                                        <td>Number of users with failed tests</td>
                                        <td><%=failStatsSize%></td>
                                    </tr>
                                    <tr class="even">
                                        <td>Total number of failed tests</td>
                                        <td><%=totalFailed%></td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr />
                            <table id="testType" width="100%">
                                <tr>
                                    <td colspan="2">Incomplete tests</td>
                                    <tr/>
                                    <tr>
                                    <td><SELECT NAME="testName" style="width:150px;">
                                            <%
                                                        List<NiseeTestType> testtypes = db.getNiseeTestTypes();
                                                        if (testtypes != null) {
                                                            for (NiseeTestType test : testtypes) {

                                                                int shapeCount = test.getNiseeTestShapeCollection().size();
                                                                int combosetCount = db.getComboset(test.getColourCombosetId()).size();
                                                                boolean perc_sizeCount = db.checkPercSize(test);
                                                                boolean form_gapsizeCount = db.checkFormatGapSize(test);
                                                                System.out.println("DEBUG="+shapeCount+", "+combosetCount+", "+perc_sizeCount+", "+ form_gapsizeCount);
                                                                if ((shapeCount < 1) || (combosetCount < 1) || (!perc_sizeCount) || (!form_gapsizeCount)) {

                                            %>
                                            <OPTION value="<%=test.getTypeId()%>"><%=test.getName()%></OPTION>
                                            <%
                                                                }
                                                            }
                                                        }
                                            %>
                                        </SELECT>
                                    <td><input type="submit"  value="Edit" name="test"/></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Unused colour combinations</td>
                                </tr>
                                <tr>
                                    <td>
                                        <SELECT NAME="colourComboSet" style="width:150px;">
                                            <%
                                                        List<NiseeColourComboSet> colourComboSets = db.getComboSets();
                                                        if (colourComboSets != null) {
                                                            for (NiseeColourComboSet colourSet : colourComboSets) {
                                                                int shapeCount = db.getCombosByColourComboSetId(colourSet.getColourCombosetId()).size();
                                                                if (shapeCount < 1) {

                                            %>
                                            <OPTION value="<%=colourSet.getColourCombosetId() %>"><%=colourSet.getName()%></OPTION>
                                            <%
                                                                }
                                                            }
                                                        }
                                            %>
                                        </SELECT>
                                    </td>
                                    <td><input type="submit"  value="Edit" name="colour"/></td>
                                </tr>
                            </table>
                        </div>
            </div>
                                        
                    <div style="width: 100%; float: left">
                        <hr />
                        <table>
                            <tr><td>
                                    <div style="line-height: 1.8em; color: #000000; font-size: 9pt">
                                        <div id="graph">Loading graph...</div>
                                    </div>
                                </td>
                                <td>
                                    <div style="line-height: 1.8em; width: 40%; color: #000000; font-size: 9pt">
                                        <div id="graph2">Loading graph...</div>
                                    </div>
                                </td</tr>
                        </table></div>
                </div></div>


            <script type="text/javascript">
                //var defaultColors = ['#FF0000', '#FFFF00', '#CCFF00', '#FF9900', '#00FFCC'];
                var myChart = new JSChart('graph', 'pie');
                myChart.setDataArray(myData);
                myChart.setTitle('Disk usage (user images)');
                myChart.setPiePosition(158, 170);
                myChart.setPieRadius(95);
                myChart.setPieUnitsFontSize(8);
                myChart.setPieUnitsColor('#000000');
                myChart.setPieValuesColor('#000000');
                myChart.setPieValuesOffset(-10);
                myChart.setTitleColor('#000000');
                myChart.setTitle('Disk usage (user images)');
                myChart.setSize(300, 321);
                //         myChart.setBackgroundImage('images/chart_bg.jpg');
                myChart.draw();

                var myChart2 = new JSChart('graph2', 'bar');
                myChart2.setTitle('Users with failed test');
                myChart2.setAxisNameX('username');
                myChart2.setAxisNameY('Failed tests');
                myChart2.setDataArray(myData2);
                myChart2.setAxisValuesNumberY(myData2[0][1]+1);
                myChart2.setAxisValuesDecimals(0);
                myChart2.setTitleColor('#000000');
                myChart2.setAxisColor('#000000')
                myChart2.setAxisNameColor('#000000');
                myChart2.setAxisValuesColor('#000000');
                myChart2.setBarValuesColor('#000000');
                myChart2.setSize(400, 321);
                //           myChart2.setBackgroundImage('images/chart_bg.jpg');
                myChart2.draw();
            </script>
        </form>
    </body>
</html>
