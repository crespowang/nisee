<%-- 
    Document   : colorComboSetDetail
    Created on : Oct 2, 2010, 10:38:23 PM
    Author     : Bing
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Colour Combos</title>
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
    </head>
    <body>
        <div id="wrap">
            <div id="title">
                <h1>Colour Combos</h1>
            </div>
            <div id="side">
                <%@ include file="side.jsp" %>
            </div>
            <div id="content">

                <table id="combos" >
                    <thead>
                        <tr>
                            <td>Foreground1</td>
                            <td>Foreground2</td>
                            <td>Background</td>
                            <td></td>
                        </tr>
                    </thead>
                    <c:forEach var="colorCombo" items="${requestScope.colorCombos}" >
                            

                        <tr>
                            <td style="background-color:${colorCombo.foreground1}"></td>
                            <td style="background-color:${colorCombo.foreground2}"></td>
                            <td style="background-color:${colorCombo.background}"></td>
                            <td><a href="RemoveColorComboFromSet?idAndSetId=${colorCombo.idAndSetId}">Delete</a></td>
                        </tr>
        



                    </c:forEach>
                </table>
                <a href="ColorComboToSet?setID=${requestScope.comboSetId}">Add New Combo</a>
            </div>
        </div>
    </body>
</html>
