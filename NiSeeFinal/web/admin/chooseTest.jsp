<%--
    Document   : chooseTest
    Created on : 28/09/2010, 9:50:33 AM
    Author     : stussy
--%>

<%@page import="niseeEntity.NiseeTestType"%>
<%@page import="javax.swing.text.Document"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Choose Test Type</title>
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
          <script type="text/javascript" src="scripts/jquery-1.4.2.min.js"></script>
   <script type="text/javascript" src="scripts/validation.js"></script>


    </head>
    <body>
        <div style="height: 50%;width:100%; min-height: 200px;"></div>
        <div>
            <form name="ChooseTest" method="post" action="..\\ChooseTest" >
                <table id="optionstable" align="center">
                    <tbody>
                        <tr>
                            <td>Test to change</td>
                            <td>
                                <SELECT NAME="testName" style="width:150px;">
                                    <%
                                                List<NiseeTestType> testtypes = (List) session.getAttribute("testtypes");
                                                if (testtypes != null) {
                                                    for (NiseeTestType test : testtypes) {
                                    %>
                                    <OPTION><%=test.getName()%></OPTION>
                                    <%
                                                    }
                                                }
                                    %>
                                </SELECT>
                            </td>
                        </tr>
                        <tr>
                            <td><input  type="submit" value="Next"/></td>
                            <td><input  type="button" value="Cancel" onclick="window.location.href='center.jsp'"/></td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </body>
</html>
