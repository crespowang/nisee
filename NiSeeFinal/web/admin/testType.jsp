<%-- 
    Document   : testType
    Created on : Oct 3, 2010, 12:38:56 AM
    Author     : Bing
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <title>Manage Colour Combos Set</title>
        <script type="text/javascript" src="scripts/admin/jscolor/jscolor.js"></script>
        <script type="text/javascript" src="scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="scripts/sprintf.js"></script>
        <script type="text/javascript" src="scripts/jquery.dataTables.min.js" ></script>
        <script type="text/javascript" src="scripts/jquery-ui-1.8.5.custom.min" ></script>
        <script type="text/javascript" src="scripts/jquery.ui.core.js"></script>
        <script type="text/javascript" src="scripts/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="scripts/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="scripts/validation.js"></script>
        <script type="text/javascript" src="scripts/jqModal.js"></script>
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
               <script type="text/javascript">
             $(document).ready(function(){
   $("#changeset").click(function(){

      if( $("#changeset").attr("value") == "Hide"){
    $("#changeset").attr( "value", "Add New Test Type");
   $("div.currentcolourset").slideUp(300).fadeOut(1000);
      }else{
             $("#changeset").attr( "value", "Hide");
  $("div.currentcolourset").slideUp(300).fadeIn(1000);
          
      }
          });
             });
        </script>
                 <script type="text/javascript">
             $(document).ready(function(){
   $("#cancels").click(function(){

        $("div.currentcolourset").slideDown(300).fadeOut(1000);


          });
             });
        </script>
              <script type="text/javascript">

            $(document).ready(function(){
                $("#addset").validate({
                    rules: {
                        name: {required:true}
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
	$("#testType").dataTable();
            });
        </script>
    </head>
    <body>
        <div id="wrap">
            <div id="title">
                <h1>Manage Test Type</h1>
            </div>
             <div id="side">
                <%@ include file="side.jsp" %>
            </div>
            <div id="content">
                    <div class="title">Existing Test Type</div>
                  <fieldset>
                <table id="testType" >
                  
         
                    <c:forEach var="test" items="${requestScope.types}" >
                        <tr>
                            <td><a href="TestTypeDetail?save=0&testID=${test.typeId}">${test.name}</a></td>
                        </tr>
                    </c:forEach>
                  
                </table>
                        </fieldset>
           
         <hr />
             <input class="button" type="submit" value="Add New Test Type" id="changeset"/>
            <form action="AddTestType" method="get" id="addset" >
              
                  <div class="currentcolourset">
                <fieldset>
                <table id="testType">
                    <thead> </thead>
                    <tr><th>Test Name</th><td><input type="text" name="name" value=""/></td></tr>
                    <tr>
                        <th>Color ComboSet Name</th>
                        <td>
                            <SELECT NAME="ComboSetId" style="width:150px;">
                                <c:forEach var="colorComboSet" items="${requestScope.niseeColourComboSets}" >
                                    <OPTION VALUE="${colorComboSet.colourCombosetId}">${colorComboSet.name}</OPTION>
                                </c:forEach>
                            </SELECT>
                        </td>
                    </tr>
                    <tr><th></th>
                        <td><input type="submit" value="Add"/> </td>
                     
                    </tr>
                </table>
                </fieldset>
                  </div>
            </form>
          </div>
        </div>
    </body>
</html>
