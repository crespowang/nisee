<%-- 
    Document   : index
    Created on : Sep 14, 2010, 11:22:04 PM
    Author     : Bing
--%>

<%@page import="java.util.List"%>
<%@page import="niseeEntity.NiseeTestType"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <SCRIPT LANGUAGE="JavaScript" TYPE="TEXT/JAVASCRIPT">
            function popUp(URL)
            {
                window.open(URL,'NiSee', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=600,height=600,left = 0,top = 0');
            }
            function ShowContent(d) {
                if(d.length < 1) { return; }
                document.getElementById(d).style.display = "block";
            }
            function HideContent(d) {
                if(d.length < 1) { return; }
                document.getElementById(d).style.display = "none";
            }
        </SCRIPT>
        <link rel="stylesheet" href="..\\niseecss.css" type="text/css" media="screen" />
    </head>
    <body>
        <div id="wrap">
            <div id="title">
                <h1>Manage test</h1>
            </div>
            <div id="content">
                <table id="optionstable">
                    <tbody>
                        <tr>
                            <td>Test name:</td>
                            <td>
                                <SELECT NAME="testName" style="width:150px;">
                                    <%
                                                List<String> testtypes = (List) session.getAttribute("testtypes");
                                                if (testtypes != null) {
                                                    for (String test : testtypes) {
                                    %>
                                    <OPTION VALUE=""><%=test%></OPTION>
                                    <%
                                                    }
                                                }
                                    %>
                                </SELECT>
                            </td>
                            <td><div id="newtestname1"><input onclick="ShowContent('newtestname2');ShowContent('newtestname3');ShowContent('newtestname4');HideContent('newtestname1')" id="submit" type="submit" value="Add new test name"/></div></td>
                            <td><div id="newtestname2"><input type="text" name="testname"></div></td>
                            <td><div id="newtestname3"><input onclick="HideContent('newtestname2');HideContent('newtestname3');HideContent('newtestname4')" id="submit" type="submit" value="Add"/></div></td>
                            <td><div id="newtestname4"><input onclick="HideContent('newtestname2');HideContent('newtestname3');HideContent('newtestname4');ShowContent('newtestname1')" id="submit" type="submit" value="Cancel"/></div></td>
                        </tr>
                        <tr>
                            <td>Colour set:</td>
                            <td>
                                <SELECT NAME="colourSet" style="width:150px;">
                                    <%
                                                List<String> coloursets = (List) session.getAttribute("coloursets");
                                                if (coloursets != null) {
                                                    for (String colour : coloursets) {
                                    %>
                                    <OPTION VALUE=""><%=colour%></OPTION>
                                    <%
                                                    }
                                                }
                                    %>
                                </SELECT>
                            <td><input id="submit" type="submit" value="Add new colour set"/></td>
                        </tr>
                        <tr><td height="3"></td></tr>
                        <tr>
                            <td COLSPAN="2">List of uploaded images</td>
                            <td><input onclick=popUp("uploadimgs.jsp") id="submit" type="submit" value="Add images"/></td>
                        </tr>
                    <c:forEach var="group" items="${sessionScope.user.niseeTestGroupCollection}" varStatus="i">
                        <c:choose>
                            <c:when test="${(i.count) % 2 == 0}">
                                <tr class="even">
                            </c:when>
                            <c:otherwise>
                                <tr class="odd">
                            </c:otherwise>
                        </c:choose>
                        <td colspan="2">img01.jpg</td>
                        <td><input onclick=popUp("createdemo.jsp") id="submit" type="submit" value="Preview and submit image"/></td>
                        </tr>
                    </c:forEach>

                    <tr><td height="3"></td></tr>
                    <tr>
                        <td><form action="center.jsp"><input id="submit" type="submit" value="Accept"/></form></td>
                        <td><form action="center.jsp"><input id="submit" type="submit" value="Cancel"/></form></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="bottom">
                <p><a href="mailto:uow321@googlegroups.com">NiSee Developing Group</a></p>
            </div>

        </div>

    </body>
</html>
