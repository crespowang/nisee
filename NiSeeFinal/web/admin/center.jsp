<%@page import="niseeEntity.NiseeColourComboSet"%>
<%@page import="java.util.List"%>
<%@page import="niseeEntity.NiseeTestType"%>
<%@page import="niseeDatabase.niseeDB"%>
<%@page import="java.util.Arrays"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
            niseeDB db = new niseeDB();
            List<Integer> numImagesOfType = Arrays.asList();
            List<NiseeTestType> testtypes = db.getNiseeTestTypes();
            request.getSession().setAttribute("testtypes", testtypes);
            request.getSession().setAttribute("numImagesOfType", numImagesOfType);
            niseeDatabase.niseeDB db2 = new niseeDB();
            List<String> coloursets = db2.getComboDistinctSetNames();
            request.getSession().setAttribute("coloursets", coloursets);
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>NiSee - test administrator</title>

                 <link type="text/css" rel="stylesheet" media="all" href="<%=request.getContextPath()%>/css/jqModal.css" />
         <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jqModal.js"></script>
           <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/validation.js"></script>
   <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.ui.datepicker.js"></script>
      <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery-ui-1.8.5.custom.min" ></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.ui.core.js"></script>
          <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/validation.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.ui.widget.js"></script>
                     <script type="text/javascript">
           $(document).ready(function() {
                $('#profilepopup').jqm({
                    focus:true,
                    ajax: '@href',
                    autoSize:true,
                    trigger: 'a#popup',
                    ajaxText: ("loading..."),
                    onHide: function(h){
                        h.o.remove(); // remove overlay
                        h.w.fadeOut(688); // hide window
                   }
                });


            });




        </script>

        <SCRIPT LANGUAGE="JavaScript" TYPE="TEXT/JAVASCRIPT">
            function popUp(URL)
            {
                window.open(URL);
            }
            function ShowContent(d) {
                if(d.length < 1) { return; }
                document.getElementById(d).style.display = "block";
            }
            function HideContent(d) {
                if(d.length < 1) { return; }
                document.getElementById(d).style.display = "none";
            }
        </SCRIPT>
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
    </head>
    <body>
       	<div id="wrap">
            <div id="title">
                <h1>NiSee - admin</h1>
            </div>
            <div id="side">
                <ul>
            
                    <li>   <a class="item" id="popup" href="profile.jsp">Profile</a></li>
                    <li><a class="item" href="logout">Logout</a></li>
                </ul>
            
            </div>


            <div class="jqmWindow" id="profilepopup"  style="width: 650px; height:500px;overflow: auto;">
                Please wait... <img src="../images/ajax-loader.gif" height="16" width="16" />
            </div>


            <div id="content">
                <h1>Welcome ${sessionScope.user.userFIRSTNAME} ${sessionScope.user.userFAMILYNAME} </h1>
                <div>
                    <table>
                        <tr>
                            <td>
                                <a onmouseout="HideContent('manageCombotip'); return true;" onmouseover="ShowContent('manageCombotip'); return true;"  type="image"  href="ColorCombos"><img  alt="manage color combo"  src="images/combomanagement.png" align="left"  border="0"/></a>
                            </td>
                            <td>
                                <div id="manageCombotip">Add and edit existing colour combinations</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a onmouseout="HideContent('manageComboSettip'); return true;" onmouseover="ShowContent('manageComboSettip'); return true;"  type="image"  href="ColorComboSet"><img alt="manage color combo set"  src="images/combosetmanagement.png" align="left"  border="0"/></a>
                            </td>
                            <td>
                                <div id="manageComboSettip">Add and edit exisiting colour combination sets</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a onmouseout="HideContent('managetip'); return true;" onmouseover="ShowContent('managetip'); return true;"  type="image"  href="TestType"><img alt="manage tests"  src="images/testmanagement.png" align="left"  border="0"/></a>
                            </td>
                            <td>
                                <div id="managetip">Add and edit existing tests</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a onmouseout="HideContent('userstip'); return true;" onmouseover="ShowContent('userstip'); return true;"  type="image"  href="manageUser"><img alt=""  src="images/usermanagement.png" align="left" border="0"/></a>
                            </td>
                            <td>
                                <div id="userstip">Add users and manage user details </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a onmouseout="HideContent('statstip'); return true;" onmouseover="ShowContent('statstip'); return true;"  type="image"  href="Statistics"><img alt=""  src="images/statistic.png" align="left" border="0"/></a>
                            </td>
                            <td>
                                <div id="statstip">View statistics about tests </div>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
            <div id="bottom">
                <p><a href="mailto:uow321@googlegroups.com">NiSee Developing Group</a></p>
            </div>

        </div>
    </body>
</html>
