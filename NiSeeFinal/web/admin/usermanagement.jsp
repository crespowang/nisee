<%-- 
    Document   : crespo_user_management
    Created on : 01/10/2010, 3:45:22 PM
    Author     : Crespo
--%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>    
    <head>
        <link type="text/css" rel="stylesheet" media="all" href="css/jqModal.css" />

        <script type="text/javascript" src="scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="scripts/jquery.dataTables.min.js" ></script>
        <script type="text/javascript" src="scripts/jquery-ui-1.8.5.custom.min.js" ></script>
        <script type="text/javascript" src="scripts/jquery.ui.core.js"></script>
        <script type="text/javascript" src="scripts/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="scripts/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="scripts/user_management_datatable.js"></script>
        <script type="text/javascript" src="scripts/validation.js"></script>
        <script type="text/javascript" src="scripts/jqModal.js"></script>
            <script type="text/javascript">
             $(document).ready(function(){
   $("#changeset").click(function(){

   $("div.currentcolourset").slideUp(300).fadeIn(1000);


          });
             });
        </script>
                 <script type="text/javascript">
             $(document).ready(function(){
   $("#cancels").click(function(){

        $("div.currentcolourset").slideDown(300).fadeOut(1000);


          });
             });
        </script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#username").keyup(function(){

                    $username = document.getElementById("username").value;
                    
                    if (this.timer) clearTimeout(this.timer);
            
                    $("#username_error").show().html('<img src="images/ajax-loader.gif" height="16" width="16" />  Checking availability...');
                
                    this.timer = setTimeout(function(){
                        $.post("formValidation", {username:$username}, function(data) {
                            if(data.username =="bad")
                                $("#username_error").show().html("That Username is unavailable");
                            else
                                $("#username_error").show().html("That Username is available");;

                        },"json");},500);
                });
            });
        </script>

         <script type="text/javascript">
           $(document).ready(function() {
                $('#ex2').jqm({
                    focus:true,
                    ajax: '@href',
                    autoSize:true,
                    trigger: 'a#popup',
                    ajaxText: ("loading..."),
                    onHide: function(h){
                        h.o.remove(); // remove overlay
                        h.w.fadeOut(688); // hide window
                   }
                });


            });

        </script>

        <script type="text/javascript">
           
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    dateFormat: 'yy-mm-dd',
                    yearRange: '1950:2009',
                    changeMonth: true,
                    changeYear: true
                });
            });
        </script>


        <script type="text/javascript">

            $(document).ready(function(){
                $("#addnewuser").validate({
                    rules: {
                        familyname: {required:true},
                        firstname: {required:true}
                    }
                });
            });
        </script>


        <style type="text/css" title="currentStyle">
            @import "css/demo_page.css";
            @import "css/demo_table.css";
            @import "css/jquery.ui.all.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Management</title>
    </head>

    <body id="dt_example">
        <div id="container">
            <hr />
            <div id="demo">
                <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">

                    <thead><tr> <th>Username</th><th>Full name</th><th>Details</th>
                        </tr></thead>
                    <tbody>
                        <c:forEach var="user" items="${requestScope.users}" varStatus="i">
                            <tr>
                                <td>${user.userUSERNAME}</td>
                                <td>${user.userFIRSTNAME} ${user.userFAMILYNAME}</td>
                                <td>
                                    <a href="viewUser?username=${user.userUSERNAME}"  id="popup">View</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
            

                    <div class="jqmWindow" id="ex2" style="width: 650px; height:500px;overflow: auto;">
                        Please wait... <img src="images/ajax-loader.gif" height="16" width="16" />
                    </div>
                    <tfoot>
                        <tr> <th>Username</th><th>Full name</th><th>Details</th></tr>
                    </tfoot>
                </table>
            </div>

        </div>

        <div id="container2">
            <hr />
   <input class="button" type="submit" value="Create New User" id="changeset"/>
   <div class="currentcolourset">
            <form  method="POST" id="addnewuser" action="addUser">
                <fieldset>
                    <table cellpadding="0" cellspacing="0" border="0" class="addnew">

                        <caption>Add new user</caption>

                        <tbody>

                            <tr><th>Username</th><td><input type="text" name="username" id="username" value="" class="required"  /></td>

                                <td id="username_error" class="username_error" colspan="2"></td>

                            </tr>
                            <tr>  <th>Gender</th><td><select name="gender"><option value="female">Female</option><option value="male">Male</option></select></td></tr>
                            <tr>
                                <th>Family name</th>
                                <td colspan="3"><input type="text"  id="cfamilyname"  name="familyname"  /></td>
                            </tr>
                            <tr>
                                <th>First name</th>
                                <td colspan="3"><input type="text"  id="cfirstname"  name="firstname" /></td>
                            </tr>
                            <tr><th>Email</th><td colspan="3"><input type="text" name="email" id="cemail" class="required email"  /></td>


                            </tr>
                            <tr><th>DOB</th>
                                <td>
                                    <p><input name="dob" id="datepicker" type="text"  size="25" /></p>

                                </td>
                            </tr>
                            <tr>
                                <TH> </TH>
                                <th><input type="submit" name="register" value="Register" id="register" /></th>
                            <td><input id="cancels" type="button" value="Cancel"/> </td></tr>
                        </tbody>


                    </table>
                </fieldset>

            </form>
          
          
        </div>
     <hr />
     <a href="Center">Back to control center</a>
        </div>
    </body>
</html>
