<%-- 
    Document   : uploadimgs
    Created on : Sep 17, 2010, 8:59:36 AM
    Author     : Bing
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../css/default.css" rel="stylesheet" type="text/css" />
        <link href="../css/uploadify.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="../scripts/swfobject.js"></script>
        <script type="text/javascript" src="../scripts/jquery.uploadify.v2.1.0.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#uploadify").uploadify({
                    'uploader'       : '../scripts/uploadify.swf',
                    'script'         : '<%=request.getContextPath()%>/UploadImage',
                    'cancelImg'      : '../images/fm/cancel.png',
                    'queueID'        : 'fileQueue',
                    'fileDesc'    : 'jpg gif bmp files',
                    'fileExt' :    '*.jpg;*.gif;*.bmp',
                    'auto'           : true,
                    'multi'          : true,
                    'buttonText'     : 'BROWSE',
                    'displayData'    : 'percentage',
                    //onComplete: function (evt, queueID, fileObj, response, data) {alert("upload OK: "+response);}
                });
            });
        </script>

        <title>Upload</title>
    </head>
    <body>
        <div class="title">
            <h5>Upload</h5>
        </div>
        <div id="fileQueue"></div>
        <input type="file" name="uploadify" id="uploadify" />
        <p>
            <a href="javascript:jQuery('#uploadify').uploadifyUpload()">Start Uploads</a>
            <a href="javascript:jQuery('#uploadify').uploadifyClearQueue()">Cancel All Uploads</a>
        </p>

    </body>
</html>
