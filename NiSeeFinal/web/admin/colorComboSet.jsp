<%-- 
    Document   : colorCombosSet
    Created on : Oct 2, 2010, 6:07:54 PM
    Author     : Bing
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Colour Combos Set</title>
        <script type="text/javascript" src="scripts/admin/jscolor/jscolor.js"></script>
        <script type="text/javascript" src="scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="scripts/sprintf.js"></script>
                <script type="text/javascript" src="scripts/validation.js"></script>
                 <script type="text/javascript">
             $(document).ready(function(){
   $("#changeset").click(function(){

   $("div.currentcolourset").slideUp(300).fadeIn(1000);


          });
             });
        </script>
                 <script type="text/javascript">
             $(document).ready(function(){
   $("#cancels").click(function(){

        $("div.currentcolourset").slideDown(300).fadeOut(1000);


          });
             });
        </script>
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
                    <script type="text/javascript">

            $(document).ready(function(){
                $("#addcolorcombo").validate({
                    rules: {
                        name: {required:true}
                    }
                });
            });
        </script>
    </head>
    <body>
        <div id="wrap">
            <div id="title">
                <h1>Colour Combo Set</h1>
            </div>
            <div id="side">
                <%@ include file="side.jsp" %>
            </div>
            <div id="content">
                  <div class="title">Existing Colour Combo</div>
                  <fieldset>
                <table id="testType">
              
                    <c:forEach var="niseeColourComboSet" items="${requestScope.niseeColourComboSets}" >
                        <tr>
                            <th><a href="ColorComboSetDetail?id=${niseeColourComboSet.colourCombosetId}">${niseeColourComboSet.name}</a></th>
                            <td><a href="DeleteColourComboSet?id=${niseeColourComboSet.id}">Delete</a></td>
                        </tr>
                    </c:forEach>
                </table>
                  </fieldset>

                <hr />
                     <input class="button" type="submit" value="Add New Colour Combo Set" id="changeset"/>
                     <div class="currentcolourset">
                <form action="AddColourComboSet" method="post" id="addcolorcombo">
                    <fieldset>
                    <table id="testType">
                        <tr>
                            <th>Colour Combo Name</th>
                            <td><input type="text" name="name" value=""/></td>
                        </tr>
                        <tr>
                            <th><input id="cancels" type="button" value="Cancel"/></th><td><input id="addColor" type="submit" value="Add"/> </td>
                        </tr>
                    </table>
                    </fieldset>
                </form>
                     </div>
            </div>
        </div>
    </body>
</html>
