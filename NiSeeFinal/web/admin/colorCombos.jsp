<%-- 
    Document   : colorCombos
    Created on : Sep 30, 2010, 3:21:01 PM
    Author     : Bing
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Colour Combos</title>
        <script type="text/javascript" src="scripts/admin/jscolor/jscolor.js"></script>
        <script type="text/javascript" src="scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="scripts/sprintf.js"></script>

               <script type="text/javascript">
             $(document).ready(function(){
   $("#changeset").click(function(){

   $("div.currentcolourset").slideUp(300).fadeIn(1000);


          });
             });
        </script>
                 <script type="text/javascript">
             $(document).ready(function(){
   $("#cancels").click(function(){

        $("div.currentcolourset").slideDown(300).fadeOut(1000);


          });
             });
        </script>

        <script type="text/javascript">
            function addcolorCombo(id) {
                //                alert(id);
                var rowInfo ="<tr><td style='background-color:#%s'></td>" +
                    "<td style='background-color:#%s'></td>" +
                    "<td style='background-color:#%s'></td>" +
                    "<td><a href='DeleteColourCombo?id=%s'>Delete</a></td></tr>";
                var name = $("input[name=name]").val();
                var fore1 = $("input[name=fore1]").val();
                var fore2 = $("input[name=fore2]").val();
                var back = $("input[name=back]").val();
                var newrow = sprintf(rowInfo, fore1, fore2, back, id);
                var selectPath = "table[id=combos] tr:last";
                //selectPath = sprintf(selectPath, roomid);
                $(newrow).insertAfter($(selectPath));
            }

            function deletecolorCombo(e) {
                //                alert(e);
            }

            $(document).ready(function() {
                $("#addColor").click(function(event){
                    var params=$('input').serialize();
                    //                    alert(params);
                    $.ajax( {
                        url: "AddColourCombo",
                        data: params,
                        type: 'get',
                        success: addcolorCombo
                    }
                );
                })
                $(".delete").click(function(event){
                    var params=$(this).attr("value");
                    alert(params);
                    $.ajax( {
                        url: "DeleteColourComboSet",
                        data: params,
                        type: 'get',
                        success: deletecolorCombo(params)
                    }
                );
                })
            })
        </script>
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
    </head>
    <body>
        <div id="wrap">
            <div id="title">
                <h1>Colour Combos</h1>
            </div>
            <div id="side">
                <%@ include file="side.jsp" %>
            </div>
            <div id="content">
                <div class="title">Existing Colour Combos</div>
                <fieldset>
                <table id="colorCombo" >
                    <thead>
                        <tr>
                            <th>Foreground1</th>
                            <th>Foreground2</th>
                            <th>Background</th>
                            <td></td>
                        </tr>
                    </thead>
                    <c:forEach var="colorCombo" items="${requestScope.colorCombos}" >
                        <tr>
                            <td style="background-color:${colorCombo.foreground1}"></td>
                            <td style="background-color:${colorCombo.foreground2}"></td>
                            <td style="background-color:${colorCombo.background}"></td>
                            <td><a href="DeleteColourCombo?id=${colorCombo.comboSetId}">Delete</a></td>
                        </tr>
                    </c:forEach>
                </table>
                </fieldset>
                <hr />

                  <input class="button" type="submit" value="Add New Colour Combo" id="changeset"/>
                  <div class="currentcolourset">
                <form action="#"  method="post">
                    <fieldset>
                    <table id="colorCombo">
                        <tr>
                            <td>Foreground1</td>
                            <td><input name="fore1" class="color" value="FF0000"/></td>
                        </tr>
                        <tr>
                            <td>Foreground2</td>
                            <td><input name="fore2" class="color" value="0000FF"/></td>
                        </tr>

                        <tr>
                            <td>Background</td>
                            <td><input name="back" class="color" value="FFFF00"/></td>
                        </tr>
                        <tr>
                            <td><input id="addColor" type="button" value="Add"/> </td>
                            <td><input id="cancels" type="button" value="Cancel"/> </td>
                        </tr>
                    </table>
                    </fieldset>
                </form>
                  </div>
            </div>
        </div>
    </body>
</html>
