<%-- 
    Document   : colorComboSetDetail
    Created on : Oct 2, 2010, 10:38:23 PM
    Author     : Bing
--%>

<%@page import="java.util.List"%>
<%@page import="niseeEntity.NiseeColourComboSet"%>
<%@page import="niseeDatabase.niseeDB"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Test Type</title>
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
        <script type="text/javascript" src="scripts/jquery-1.4.2.min.js"></script>
             <script type="text/javascript">
             $(document).ready(function(){
   $("#changeset").click(function(){
       if( $("#changeset").attr("value") == "Hide"){

               $("#changeset").attr( "value", "Manage Colour Combo Set");
        $("div.currentcolourset").slideUp(300).fadeOut(1000);
       }else{
       $("#changeset").attr( "value", "Hide")
        $("div.currentcolourset").slideUp(300).fadeIn(1000);
       }
          });
             });
        </script>

        <SCRIPT LANGUAGE="JavaScript" TYPE="TEXT/JAVASCRIPT">
            function popUp(URL)
            {
                window.open(URL,'NiSee', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=600,height=600,left = 0,top = 0');
            }
            function ShowContent(d) {
                if(d.length < 1) { return; }
                document.getElementById(d).style.display = "block";
            }
            function HideContent(d) {
                if(d.length < 1) { return; }
                document.getElementById(d).style.display = "none";
            }
        </SCRIPT>

    </head>
    <body>
        <div id="wrap">
            <div id="title">
                <h1>${requestScope.testType.name}</h1>
            </div>
            <div id="side">
                <%@ include file="side.jsp" %>
            </div>
            <div class="title" >Current Images</div>
            <fieldset>
                <c:forEach var="imageFile" items="${requestScope.imageFiles}" >
                    <a href="GenerateShape?imagePath=${imageFile.path}&testTypeID=${requestScope.testType.id}">
                        <img alt="${imageFile.name}"  src="${requestScope.imageSrc}${imageFile.path}" height="100" width="100" />
                    </a>
                </c:forEach>
            </fieldset>
            <div class="title"><a href="UploadTestImage?testID=${requestScope.testType.id}" id="addnewimage">Add New Image</a></div>
<hr />
  

           
                
            <table id="testType">
                <tr><td><input class="button" type="submit" value="Manage Colour Combo Set" id="changeset"/></td></tr>
            </table>
                    <div class="currentcolourset">
                          <fieldset>
                   <table id="testType">
                <form action="TestTypeDetail?save=1&testID=${requestScope.testType.id}" method="POST">
                    
                    <tr>
                    <th>Current Colour Combo Set</th>
                    <td>

                <select name="colourset" id="colours">
                    <c:forEach var="colourset" items="${requestScope.coloursets}" >
                        <c:choose>
                            <c:when test="${colourset.colourCombosetId == requestScope.currentsetid }">
                                <option value="${colourset.colourCombosetId}" selected>${colourset.name}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${colourset.colourCombosetId}" >${colourset.name}</option>
                            </c:otherwise>

                        </c:choose>

                    </c:forEach>
                </select>
                    </td>
                    </tr>
                    <tr>
                        <td><input class="button" type="submit" value="Save" name="save"/></td>
                </tr>
                     
            </form>
              
                   
                 </table>
                      </fieldset>
                          </div>
                       </div>
                
 

    </body>
</html>