<%-- 
    Document   : creatdemo
    Created on : Sep 16, 2010, 6:03:42 PM
    Author     : Bing
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet"  href="<%=request.getContextPath()%>/css/fm.css" type="text/css" charset="utf-8" title="main" />
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery-ui-1.8.4.custom.min.js"></script>
        <script type="text/javascript"  src="<%=request.getContextPath()%>/scripts/jquery.fm.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/admin/createdemo.js"></script>

    </head>
    <body>
        <div class="title">
            <h5>Generate Shapes</h5>
        </div>
        <div>
            <table>
                <tr width="400">
                    <td colspan="8"><img src='../GetDemo' id="img" alt="alt"/></td>
                    <td>
                        <table>
                <tr>
                    <td>Percentage L</td>
                    <td><input name="perc1" type="text" readonly="yes" value="20" /><img src="<%=request.getContextPath()%>/images/fm/pic_biaoche.gif" align="absmiddle" id="perc1" /></td>
                    <td>Size L</td>
                    <td>
                        <input name="size1" type="text" readonly="yes" value="15"/><img src="<%=request.getContextPath()%>/images/fm/pic_biaoche.gif" align="absmiddle" id="size1" />
                    </td>
                </tr>
                <tr>
                    <td>Percentage M</td>
                    <td>
                        <input name="perc2" type="text" readonly="yes" value="27" /><img src="<%=request.getContextPath()%>/images/fm/pic_biaoche.gif" align="absmiddle" id="perc2" />
                    </td>
                    <td>Size M</td>
                    <td>
                        <input name="size2" type="text" readonly="yes" value="10"/><img src="<%=request.getContextPath()%>/images/fm/pic_biaoche.gif" align="absmiddle" id="size2" />
                    </td>
                </tr>
                <tr>
                    <td>Percentage S</td>
                    <td>
                        <input name="perc3" type="text" readonly="yes" value="29" /><img src="<%=request.getContextPath()%>/images/fm/pic_biaoche.gif" align="absmiddle" id="perc3" />
                    </td>
                    <td>Size S</td>
                    <td>
                        <input name="size3" type="text" readonly="yes" value="5"/><img src="<%=request.getContextPath()%>/images/fm/pic_biaoche.gif" align="absmiddle" id="size3" />
                    </td>
                </tr>
                <tr>
                    <td> Gap: <input name="gap" type="text" value="1"/> </td>
                    <td> <input name="format" type="radio" value="1"/>Square<br/>
                        <input name="format" type="radio" value="2"/>Circle
                    </td>
                </tr>

                <tr><td>Answer:</td><td><input type="text" name="answer"/></td></tr>
                <tr><td>Type: </td></tr>
                <tr><td><a id="refresh" href="#">Refresh</a>
                        <a id="submit" href="#">That's it!</a></td></tr>

                        </table>
                    </td></tr>
            </table>
        </div>
    </body>
</html>
