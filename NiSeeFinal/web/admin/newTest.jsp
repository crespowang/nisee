<%--
    Document   : addTestType
    Created on : 28/09/2010, 6:22:22 PM
    Author     : stussy
--%>

<%@page import="niseeEntity.NiseeColourComboSet"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <SCRIPT LANGUAGE="JavaScript" TYPE="TEXT/JAVASCRIPT">
            function popUp(URL)
            {
                window.open(URL,'NiSee', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=600,height=600,left = 0,top = 0');
            }
            function ShowContent(d) {
                if(d.length < 1) { return; }
                document.getElementById(d).style.display = "block";
            }
            function HideContent(d) {
                if(d.length < 1) { return; }
                document.getElementById(d).style.display = "none";
            }
        </SCRIPT>
        <link rel="stylesheet" href="..\\niseecss.css" type="text/css" media="screen" />
    </head>
    <body><center>
            <form name="ChooseTest" method="post" action="center.jsp">
                <div id="wrap" style="width: 100%;">
                    <div id="title" style="width:80%;height:10%;">
                        <h1 align="center">New test</h1>
                    </div>
                    <br>
                    <div id="testleft" style="width:100%; height:80%; float:left;min-height: 350px;">
                        <table id="optionstable">
                            <tbody>
                                <tr>
                                    <td>Test name:</td>
                                    <td>
                                        <input name="mynewtestname" type="text" value="">
                                    </td>
                                    <td><input id="submit" type="button" onclick="" VALUE="Validate"/></td>

                                </tr>

                                <tr>
                                    <td>Colour set:</td>
                                    <td>
                                        <SELECT NAME="colourSet" style="width:150px;" id="qwe">
                                            <%
                                                        List<String> coloursets = (List) session.getAttribute("coloursets");
                                                        if (coloursets != null) {
                                                            for (String colourset : coloursets) {
                                            %>
                                            <OPTION><%=colourset%></OPTION>
                                            <%
                                                            }
                                                        }
                                            %>
                                        </SELECT>
                                    </td>
                                    <td><input id="submit" type="button" onclick="ShowContent('testright'); HideContent('testleft');" VALUE="Add colourset"/></td>
                                </tr>
                                <tr><td height="3"></td></tr>
                                <tr>
                                    <td COLSPAN="2"><h2>List of uploaded images</h2></td>
                                    <td><input onclick=popUp("uploadimgs.jsp") id="submit" type="button" value="Upload images"/></td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="overflow: auto;height: 250px;width:50%">
                            <table id="optionstable" width="100%">
                                <tbody >
                                    <tr class="even"><td>sample</td><td>img01.jpg</td>
                                        <td><input onclick="ShowContent('testright2'); HideContent('testleft');" id="submit" type="button" value="Preview/submit"/></td>
                                    </tr>
                                    <tr class="odd"><td>sample</td><td>img02.jpg</td>
                                        <td><input onclick="ShowContent('testright2'); HideContent('testleft');" id="submit" type="button" value="Preview/submit"/></td>
                                    </tr>
                                    <tr class="even"><td>sample</td><td>img01.jpg</td>
                                        <td><input onclick="ShowContent('testright2'); HideContent('testleft');" id="submit" type="button" value="Preview/submit"/></td>
                                    </tr>
                                    <tr class="odd"><td>sample</td><td>img02.jpg</td>
                                        <td><input onclick="ShowContent('testright2'); HideContent('testleft');" id="submit" type="button" value="Preview/submit"/></td>
                                    </tr>
                                    <tr class="even"><td>sample</td><td>img01.jpg</td>
                                        <td><input onclick="ShowContent('testright2'); HideContent('testleft');" id="submit" type="button" value="Preview/submit"/></td>
                                    </tr>
                                    <tr class="odd"><td>sample</td><td>img02.jpg</td>
                                        <td><input onclick="ShowContent('testright2'); HideContent('testleft');" id="submit" type="button" value="Preview/submit"/></td>
                                    </tr>
                                    <tr class="even"><td>sample</td><td>img01.jpg</td>
                                        <td><input onclick="ShowContent('testright2'); HideContent('testleft');" id="submit" type="button" value="Preview/submit"/></td>
                                    </tr>
                                <c:choose>
                                    <c:when test="${(i.count) % 2 == 0}">
                                        <tr class="even">
                                    </c:when>
                                    <c:otherwise>
                                        <tr class="odd">
                                    </c:otherwise>
                                </c:choose><td>sample</td>
                                <td>img08.jpg</td>
                                <td><input onclick="ShowContent('testright2'); HideContent('testright');" id="submit" type="button" value="Preview/submit"/></td>
                                </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <input id="submit" type="submit" value="Accept"/>
                        <input id="submit" type="submit" value="Cancel"/>
                    </div>
                    <div id="testright" style="width:100%; height:80%; float:right;min-height: 350px;">
                        COLOUR SET DETAILS<br>
                        Add the colour set to database and refresh browser to add to dropdown<br>
                        <td><input id="submit" type="button" value="Accept" onclick="HideContent('testright');ShowContent('testleft');"/></td>
                        <td><input id="submit" type="button" value="Cancel" onclick="HideContent('testright');ShowContent('testleft');"/></td>
                    </div>
                    <div id="testright2" style="width:100%; height:80%; float:right;min-height: 350px;">
                        <iframe src ="createdemo.jsp" width="100%" height="300">
                            <p>Your browser does not support iframes.</p>
                        </iframe>
                        ADD SHAPE DETAILS
                        <td><input id="submit" type="button" value="Accept" onclick="HideContent('testright2');ShowContent('testleft');"/></td>
                        <td><input id="submit" type="button" value="Cancel" onclick="HideContent('testright2');ShowContent('testleft');"/></td>
                    </div>
                    <div id="bottom" style="width:100%; height:5%; float:right">
                        <p><a href="mailto:uow321@googlegroups.com">NiSee Developing Group</a></p>
                    </div>
                </div>


            </form>
        </center>
    </body>
</html>
