<%-- 
    Document   : addColorComboToSet
    Created on : Oct 6, 2010, 6:08:42 AM
    Author     : Bing
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Colour Combos</title>
        <script type="text/javascript" src="scripts/admin/jscolor/jscolor.js"></script>
        <script type="text/javascript" src="scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="scripts/sprintf.js"></script>
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
    </head>
    <body>
        <div id="wrap">
            <div id="title">
                <h1>Colour Combos</h1>
            </div>
            <div id="side">
                <%@ include file="side.jsp" %>
            </div>
            <div id="content">

                <form action="AddColorComboToSet"  method="post">
                       <fieldset>
                    <table id="testType">
                     
                        <thead>
                            <tr>
                                <td>Foreground1</td>
                                <td>Foreground2</td>
                                <td>Background</td>
                                <td></td>
                            </tr>
                        </thead>
                        <input type="hidden" name="setID" value="${requestScope.setID}" />
                        <c:forEach var="colorCombo" items="${requestScope.colorCombos}" >
                            <tr>
                                <td style="background-color:${colorCombo.foreground1}"></td>
                                <td style="background-color:${colorCombo.foreground2}"></td>
                                <td style="background-color:${colorCombo.background}"></td>
                                <td><input name="colorComboSet" type="checkbox" value="${colorCombo.id}" safari=1 /></td>
                            </tr>
                        </c:forEach>
                          
                  
                      </table>
                             </fieldset>
                        <hr />
                        <table id="testType">
                            <tr><td><input type="submit" name="submit" value="Add"/></td><td><input type="button"  value="Cancel" onclick="window.location='ColorComboSetDetail?id=${requestScope.setID}'" /></td></tr>
                        </table>
                            
                </form>
            </div>
        </div>
    </body>
</html>
