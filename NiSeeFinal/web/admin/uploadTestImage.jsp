<%-- 
    Document   : uploadimgs
    Created on : Sep 17, 2010, 8:59:36 AM
    Author     : Bing
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/default.css" rel="stylesheet" type="text/css" />
        <link href="css/uploadify.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
        <script type="text/javascript" src="scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="scripts/swfobject.js"></script>
        <script type="text/javascript" src="scripts/jquery.uploadify.v2.1.0.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#uploadify").uploadify({
                    'uploader'       : 'scripts/uploadify.swf',
                    'script'         : 'UploadImage',
                    'cancelImg'      : 'images/fm/cancel.png',
                    'queueID'        : 'fileQueue',
                    'folder'         : '${requestScope.testID}',
                    'fileDesc'    : '*.jpg;*.gif;*.bmp',
                    'fileExt' :    '*.jpg;*.gif;*.bmp',
                    'multi'          : true,
                    'buttonText'     : 'BROWSE'
//                    'onComplete': function (evt, queueID, fileObj, response, data) {alert("upload OK: "+response);}
                });
            });
        </script>

        <title>Upload</title>
    </head>
    <body>
        <div id="wrap">
            <div id="title">
                <h1>Upload</h1>
            </div>
            <div id="side">
                <%@ include file="side.jsp" %>
            </div>
                <div id="fileQueue"></div>
                <input type="file" name="uploadify" id="uploadify" />
                <table id="testType">
                      <tr><td><input type="button" value="Upload" onclick="javascript:jQuery('#uploadify').uploadifyUpload()" />  </td></tr>
                    <tr><td>    <input type="button" value="Cancel All" onclick="javascript:jQuery('#uploadify').uploadifyClearQueue()" /></td></tr>
                  <tr><td>     <input type="button" value="Continue" onclick="window.location='TestTypeDetail?testID=${requestScope.testID}'" /></td></tr>
               
                </table>
        </div>
    </body>
</html>
