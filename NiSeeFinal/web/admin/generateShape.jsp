<%-- 
    Document   : creatdemo
    Created on : Sep 16, 2010, 6:03:42 PM
    Author     : Bing
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Generate Shape</title>
        <link type="text/css" href="css/ui-lightness/jquery-ui-1.8.5.custom.css" rel="stylesheet" />
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
        <script type="text/javascript" src="scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="scripts/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="scripts/validation.js"></script>
        <script type="text/javascript" src="scripts/admin/createdemo.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $("input#radiochooseset").change(function(){
                    $("a#refresh").fadeIn(1000);
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("a#refresh").click(function(){
                    $("a#saves").delay(3000).fadeIn(1000);
                });
            });
        </script>
        <style type="text/css">
            #demo-frame > div.demo { padding: 10px !important; }
            #perc-range {
                width: 240px;
            }
            #size-range span {
                width: 120px; float:left; margin:15px
            }
        </style>

        <SCRIPT LANGUAGE="JavaScript" TYPE="TEXT/JAVASCRIPT">
            function setValues() {
                document.getElementById('gap').value = document.getElementById('thegap').value;
                document.getElementById('format').value = document.getElementById('theformat').value;
            }
        </SCRIPT>

    </head>
    <body>
        <div id="wrap">
            <div id="title">
                <h1>Generate Shapes</h1>
            </div>

            <div id="content">
                <input name="testTypeID" value="${requestScope.testTypeID}" type="hidden" />
                <input name="perc1" id="perc1" type="hidden" />
                <input name="perc2" id="perc2" type="hidden" />
                <input name="perc3" id="perc3" type="hidden" />

                <input name="size1" id="size1" type="hidden" value="20" />
                <input name="size2" id="size2" type="hidden" value="15"  />
                <input name="size3" id="size3" type="hidden"  value="10" />
                <fieldset>
                    <table  id="generateshape">
                        <tr>
                            <td ROWSPAN="6"  VALIGN="top"><img src='${requestScope.imageSrc}' id="img"  height="300" width="300"  alt="alt"/></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="hidden" value="${requestScope.imageSrc}" name="imageSrc" />
                                <label for="percentage">Percentage</label>
                                <input type="text" id="percentage" style="border:0; color:#f6931f; font-weight:bold;" />
                                <div id="perc-range" style="height:10px;"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="size">Size</label>
                                <input type="text" id="size" style="border:0; color:#f6931f; font-weight:bold;" />
                                <div id="size-range" style="height: 100px;width:200px;">
                                    <span size="#size1">20</span>
                                    <span size="#size2">15</span>
                                    <span size="#size3">10</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Gap &nbsp;</label>
                                <SELECT id="thegap">
                                    <OPTION value="0">Smaller</OPTION>
                                    <OPTION value="1">Small</OPTION>
                                    <OPTION value="2">Medium</OPTION>
                                    <OPTION value="3">Larger</OPTION>
                                    <OPTION value="4">Larger</OPTION>
                                </SELECT>
                                <input name="gap" id="gap" type="hidden"/>
                        </tr>
                        <tr>
                            <td>
                                <label>Type</label>
                                <select name="theformat" id="theformat">
                                    <option value="1">Square</option>
                                    <option value="2">Circle</option>
                                    <input name="format" id="format" type="hidden"/>
                                    <!--                            <input name="format" type="radio" value="1"/>Square
                                                                <input name="format" type="radio" value="2"/>Circle-->
                                </select>
                            </td>
                        </tr>
                        <tr><td colspan="2">Answer:<br/><input type="text" name="answer" /></td></tr>
                        <Tr><td> <button id="chooseColorCombo">Choose Preview Color</button></td><td>
                                <div id="choosedColorCombo">
                                </div></td></Tr>


                    </table>
                </fieldset>
                <hr />
                <table id="generateshape">
                    <tr>

                        <td colspan="3" align="center">
                            <a id="refresh" class="button" onclick="setValues()" href="#" style="display: none">Generate</a>
                            <a id="saves" href="#" class="button" style="display: none">Save</a>
                            <a id="cancels" href="TestTypeDetail?save=0&testID=${requestScope.testTypeID}"  class="button">Continue</a>
                        </td></tr>
                </table>
            </div>
        </div>


        <div id="colorCombo">
            <table id="combos">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Foreground1</td>
                        <td>Foreground2</td>
                        <td>Background</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="colorCombo" items="${requestScope.colorCombos}" >
                        <tr style="width: 900px">
                            <td>${colorCombo.id}</td>
                            <td style="background-color:${colorCombo.foreground1}"></td>
                            <td style="background-color:${colorCombo.foreground2}"></td>
                            <td style="background-color:${colorCombo.background}"></td>
                            <td><input type="radio" id="radiochooseset" name="colorComboID" value="${colorCombo.id}"></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>