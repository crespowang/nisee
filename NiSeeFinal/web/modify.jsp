<%-- 
    Document   : modify
    Created on : 04/09/2010, 11:16:35 PM
    Author     : Crespo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
              <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/validation.js"></script>
                    <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery-1.4.2.min.js"></script>
         <title> ${sessionScope.user.userFIRSTNAME} ${sessionScope.user.userFAMILYNAME}</title>
    </head>
    <body onunload="opener.location.reload()">
        <form action="saveProfile" method="POST" onsubmit="javascript:self.close()">
            <table id="profiletable">
                <tr><th>Username</th><td> <input type="text" value="${sessionScope.user.userUSERNAME}" disabled /></td></tr>
                <tr><th>Surname</th><td> <input type="text" name="userFAMILYNAME" value="${sessionScope.user.userFAMILYNAME}" /></td></tr>
                <tr><th>First name</th><td> <input type="text" name="userFIRSTNAME" value="${sessionScope.user.userFIRSTNAME}" /></td></tr>
                <tr><th>Date of Birth</th><td> <input type="text" name="userDOB" value="${sessionScope.user.userDOB}" /></td></tr>
                <tr><th>Gender</th><td> <input type="text" name="userGENDER" value="${sessionScope.user.userGENDER}" /></td></tr>
                <tr><th>Email</th><td> <input type="text" name="userEMAIL" class="required email"  value="${sessionScope.user.userEMAIL}" /></td></tr>
                <tr><td colspan="2"><input id="submit" type="submit" value="Submit" /></td></tr>
        </table>
            
        </form>
    </body>
</html>
