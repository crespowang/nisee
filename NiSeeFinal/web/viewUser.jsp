<%-- 
    Document   : viewUser
    Created on  : 07/10/2010, 5:14:12 PM
    Author     : Crespo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
        <title>${requestScope.user.userUSERNAME} Details</title>

    </head>
    <body>
        <h1>${requestScope.user.userFIRSTNAME} ${requestScope.user.userFAMILYNAME}</h1>
        <table width="400"  cellpadding="0" cellspacing="0" border="0" id="profiletable">
            <tbody>
                <tr>
                    <th>First name</th> <td>${requestScope.user.userFIRSTNAME}</td>
                </tr>
                
                <tr>
                    <th>Last name</th> <td>${requestScope.user.userFAMILYNAME}</td>
                </tr>
                
                <tr>
                    <th>Date of birthday</th> <td>${requestScope.user.userDOB}</td>
                </tr>

                <tr>
                    <th>Gender</th> <td>${requestScope.user.userGENDER}</td>
                </tr>
                
                <tr>
                    <th>Email</th> <td>${requestScope.user.userEMAIL}</td>
                </tr>

            </tbody>
            
        </table>
                 <hr />
                <h2>Test History</h2>
                <form action="deleteHistoryAdmin" name="delete" method="POST" >
                <table id="historytable">
                    <thead><tr><th width="100px">Test No</th><th  width="100px">Date</th><th  width="100px">Result</th><th  width="100px">Review</th><th  width="100px">Delete</th></tr></thead>
                    <tbody>
                        <c:forEach var="group" items="${requestScope.user.niseeTestGroupCollection}" varStatus="i">
                            <c:choose>
                                <c:when test="${(i.count) % 2 == 0}">
                                    <tr class="even">
                                    </c:when>
                                    <c:otherwise>
                                    <tr class="odd">
                                    </c:otherwise>
                                </c:choose>

                                <td>${group.testGROUP}</td>
                                <td>${group.testDATE}</td>
                                  <td>${group.testDIAGNOSIS}</td>
                                <td><a href="reviewTestAdmin?groupid=${group.testGroupId}&username=${requestScope.user.userUSERNAME}" target="blank">Review</a></td>
                                <td><input type="checkbox" name="todelete" value="${group.testGroupId}"></td>
                            </tr>
                        </c:forEach>
                            <tr><td><input type="submit" value="Discard" /></td></tr>
                    </tbody>
                </table>
        
             
                 <hr />
                 <p><input type="button" value="Close" class="jqmClose"/></p>
                    </form>
    </body>
</html>
