<%-- 
    Document   : setting
    Created on : 17/09/2010, 11:15:00 AM
    Author     : Crespo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Setting</title>
         <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
            <script type="application/javascript" src="scripts/popup.js"></script>
            <script type="text/javascript">
                $(document).ready(function() {

                 var $boxChecked = 0;
      $('input#deletecheckbox').change(function() {
         if ($(this).attr('checked')) // We have a valid click
         {
               $boxChecked++;
         }
         else{
                $boxChecked--;
         }
         
      });


                $('input#deletecheckbox').change(function () {
        if($boxChecked == 0){
                 $('input#deletebutton').fadeOut(500);
                        return;
        }else{
             $('input#deletebutton').fadeIn(500);
                        return;

        }

     

    //Here do the stuff you want to do when 'unchecked'
                });
                });
            </script>
    </head>
    <body onunload="opener.location.reload()">
<div id="content">
                 <h2>Your test history</h2>
                 <form action="deleteHistory" name="delete" method="POST"  onsubmit="javascript:self.close()">
                   <fieldset>
                         
                     <table id="settingtable">
                            
                         <thead><tr><th>Test No</th><th>Date</th><th>Delete</th><th></th></tr></thead>
                        
                     <tbody>
                         <c:choose>
                             <c:when test="${fn:length(sessionScope.user.niseeTestGroupCollection) == '0'}">
                                 <tr>
                                     <td colspan="3">You don't have any history</td>
                         </tr>

                             </c:when>
                           <c:otherwise>
                     <c:forEach var="group" items="${sessionScope.user.niseeTestGroupCollection}" varStatus="i">
                         <tr>
                            <td>${group.testGROUP}</td>
                             <td>${group.testDATE}</td>
                             <c:choose>
                                  <c:when test="${group.testDIAGNOSIS == 'FAIL'}">
                                      <td id="delete">Cannot delete failed test</td>
                                  </c:when>
                                  <c:otherwise>
                                      <td id="delete"><input type="checkbox" name="todelete" value="${group.testGroupId}" id="deletecheckbox"></td>
                                          </c:otherwise>
                             </c:choose>
                         </tr>
                     </c:forEach>
                           
                         <tr><td><input type="submit" value="Delete" id="deletebutton"/></td><td><input type="button" value="Close" class="jqmClose"/></td></tr>
                           </c:otherwise>
                           </c:choose>
                     </tbody>
                    
                 </table>
                    </fieldset>
                 </form>

	</div>

    </body>
</html>
