<%--
    Document   : chooseTest
    Created on : 17/09/2010, 12:19:26 PM
    Author     : Crespo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
        <script src="scripts/progressbar.js" type="text/javascript"></script>
       <script type="text/javascript" src="scripts/jquery-1.4.2.min.js"></script>
   
        <script type="text/javascript">

jQuery(function($){
        $("#startnewtest").click(function(){
        $("div#progressbar").slideUp(300).delay(100).fadeIn(400);
        $("div.notificationarea").slideUp(300).fadeOut(400);
     
        });
  
});
        </script>
        <title>Choose Test</title>
    </head>
    <body>
      
           <form action="prepareImage" method="GET" >
                 <fieldset>
                      
                              
        <table  id="chooseTestTable" align="center">
              <c:choose>
               <c:when test="${fn:length(requestScope.typenames) != '0'}">
                <tr>
                    <th><div class ="notificationarea"> Please select test type</div></th>
                    <td>
                        <div class ="notificationarea">
                         
                             


                        <SELECT NAME="testName">
                                 <c:forEach var="typename" items="${requestScope.typenames}" varStatus="i">
                                <OPTION value="${typename.type_id}">${typename.name}   - Colour Combo Combination: ${typename.num} </OPTION>
                            </c:forEach>
                          
                        </SELECT>
                         
                         

                        </div>
                    </td>
                    <td><input type="hidden" name="type" value="${requestScope.type}" /></td>
                </tr>

                <tr>
                    <td colspan="2">
                <div class="notificationarea">After you click Start button ,you will be given 5 seconds for each image to identify it</div>
                    </td>
                </tr>
                <tr>
                    <td><div class ="notificationarea"> <input type="submit" value="Start" name="submit" id="startnewtest" /></div></td>
                    <td></td>
                </tr>
                <tr>
                       <td><div class ="notificationarea"><input  type="button" value="Cancel" name="cancel" onclick="window.close()" /></div></td>

                </tr>
                
            </c:when>
                  <c:otherwise>
                      <tr> <td><div align="center">New test is not avaliable now . Please ask administrator to create test for you</div></td></tr>
                      <tr> <td> <div align="center"><input  type="button" value="Cancel" name="cancel" onclick="window.close()" /></div></td></tr>
                  </c:otherwise>
                   </c:choose>
        </table>
                 
                              
                
                <div id="progressbar" align="center"><p>We are generating images for you</p><img src="images/ajax-loader-bar.gif" alt=""/></div>
                  </fieldset>
                  </form>
    </body>
</html>
