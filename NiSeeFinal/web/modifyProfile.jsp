

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />

        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.dataTables.min.js" ></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery-ui-1.8.5.custom.min" ></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.ui.core.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/user_management_datatable.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/validation.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jqModal.js"></script>

        <script type="text/javascript">

	$(document).ready(function() {
            $("#datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
                yearRange: '1950:2009',
                autoSize: true,
                changeMonth: true,
		changeYear: true
            });
        });
    </script>
            <script type="text/javascript">

            $(document).ready(function(){
                $("#modifyuser").validate({
                rules: {
	          userFAMILYNAME: {required:true},
	          userFIRSTNAME: {required:true}
                }
                });
            });
        </script>
                <style type="text/css" title="currentStyle">
            @import "css/jquery.ui.all.css";
        </style>
        <title> ${sessionScope.user.userFIRSTNAME} ${sessionScope.user.userFAMILYNAME}</title>
    </head>
    <body>
         <form action="saveProfile" id="modifyuser" method="POST" >
                  <fieldset>
                      <table id="profiletable" align="center">
            <tbody>
              <tr><th>Username</th><td><input type="text" value="${sessionScope.user.userUSERNAME}" disabled /></td></tr>
              <tr><th>Surname</th><td colspan="3"><input type="text" id="cuserFAMILYNAME" name="userFAMILYNAME" value="${sessionScope.user.userFAMILYNAME}" /></td></tr>
              <tr><th>First name</th><td>  <input type="text" id="cuserFIRSTNAME" name="userFIRSTNAME" value="${sessionScope.user.userFIRSTNAME}" /></td></tr>
                <tr><th>Date of Birth</th><td> <input type="text" name="userDOB" id="datepicker"  value="${sessionScope.user.userDOB}" /> </td></tr>
            
                  <tr><th>Gender</th><td>



                          <select name="userGENDER">
                                         <c:choose>
                                <c:when test="${sessionScope.user.userGENDER == 'Male'}">
                                    <option value="Female">Female</option><option value="Male" selected>Male</option>
                                </c:when>
                                    <c:otherwise>
                                        <option value="Female" selected>Female</option><option value="Male">Male</option>
                                    </c:otherwise>
                                </c:choose>


                          </select>

                      </td></tr>
                  <tr><th>Email</th><td><input type="text" name="userEMAIL" id="cuserEMAIL" class="required email"  value="${sessionScope.user.userEMAIL}" /></td></tr>
            </tbody>

             <tfoot>

                     <c:choose>
                                <c:when test="${sessionScope.user.userPRIVILEGES == '1'}">
  <tr><th><input type="submit" value="Submit" /></th><td><input type="button" value="Close" onclick="window.location='Center'"></td></tr>
                                </c:when>
                                    <c:otherwise>
                                 <tr><th><input type="submit" value="Submit" /></th><td><input type="button" value="Close" onclick="window.location='center.jsp'"></td></tr>
                                    </c:otherwise>
                                </c:choose>

               
        </tfoot>

        </table>
                      </fieldset>
         </form>
    </body>
</html>
