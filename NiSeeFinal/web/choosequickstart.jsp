<%--
    Document   : chooseTest
    Created on : 17/09/2010, 12:19:26 PM
    Author     : Crespo
--%>

<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
          <script type="text/javascript" src="scripts/jquery-1.4.2.min.js"></script>
          <script type="text/javascript" src="scripts/validation.js"></script>
                 <script type="text/javascript">

            $(document).ready(function(){
                $("#chooseTestForm").validate({
                    rules: {
                        imagesnumber:{required:true,digits:true,min:5,max:20}
                    }
                });
            });
        </script>



        <title>Choose Test</title>
    </head>
    <body>

            <form action="prepareImage" method="GET" id="chooseTestForm">
                <fieldset>
                    <table id="chooseTestTable" align="center">
                <tr>
                    <th>Test type</th>
                    <td>
                        <SELECT NAME="testName" >
                            <c:forEach var="typename" items="${requestScope.typenames}" varStatus="i">
                                <OPTION value="${typename.type_id}">${typename.name}   - Available Images: ${typename.num} </OPTION>
                            </c:forEach>
             
                        </SELECT>

                    </td>
                </tr>
             
              
                <tr>
                    <th>Number of images</th>
                    <td><input type="text" name="imagesnumber" value=""  /></td>
                    <td><input type="hidden" name="type" value="${requestScope.type}" /></td>
                </tr>
                <tr>   <td><input type="submit" value="Start" /></td></tr>
                <tr>   <td><input type="button" value="Cancel" onclick="window.close()" /></td></tr>
                   </table>
                </fieldset>
            </form>

    </body>
</html>
