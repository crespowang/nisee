<%-- 
    Document   : summary
    Created on : 04/09/2010, 3:20:59 PM
    Author     : Crespo
--%>


<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <script type="application/javascript" src="scripts/popup.js"></script>
           <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
        <title>Summary</title>
    </head>
    <body onunload="opener.location.reload()">
        <h1>You have finished all test images</h1>
        <form action="keepAnswer" method="POST" >
            <table id ="summarytable" align="center">
            <tr><td>Image</td><td>Your answer</td><td>Correct Answers</td></tr>
            <c:forEach var="tab" items="${requestScope.tabs}" >
                <tr>
                    <td><img alt="" height="150" width="150"  src="getImage?id=${tab.image_id}" /> </td>
                    <td>${tab.user_ans}</td>
                 <td>
                    <c:forEach var="answers" items="${tab.correct_ans}" >
                     <p>${answers}</p>
                    </c:forEach>
            </td>
                <td>
                <c:choose>
		<c:when test="${tab.compare}">
                    <img alt=""  src="images/tick.png" />
		</c:when>
		<c:otherwise>
                    <img alt=""  src="images/error.png" />
		    </c:otherwise>
		</c:choose>
                </td>
                </tr>
            </c:forEach>
              <tr><td colspan="3"><h2>Do you wish to store this result?</h2></td></tr>
              <tr>
                  <td><input type="submit" value="Save" /></td>
                  <td><input type="button" value="Discard" onclick="window.close()"/></td>
              </tr>
             </table>
             </form>
               
    </body>
</html>
