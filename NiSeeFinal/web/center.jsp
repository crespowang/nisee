<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>NiSee</title>
         <link type="text/css" rel="stylesheet" media="all" href="<%=request.getContextPath()%>/css/jqModal.css" />
         <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jqModal.js"></script>
           <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/validation.js"></script>
   <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.ui.datepicker.js"></script>
      <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery-ui-1.8.5.custom.min" ></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.ui.core.js"></script>
          <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/validation.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.ui.widget.js"></script>

               <script type="text/javascript">
             $(document).ready(function(){
   $("#changeset").click(function(){
       if( $("#changeset").attr("value") == "Hide"){

               $("#changeset").attr( "value", "Show my test history");
        $("div.currentcolourset").slideUp(300).fadeOut(1000);
       }else{
       $("#changeset").attr( "value", "Hide")
        $("div.currentcolourset").slideUp(300).fadeIn(1000);
       }
          });
             });
        </script>
                 <script type="text/javascript">
             $(document).ready(function(){
   $("#cancels").click(function(){

        $("div.currentcolourset").slideDown(300).fadeOut(1000);


          });
             });
        </script>

             <script type="text/javascript">
           $(document).ready(function() {
                $('#profilepopup').jqm({
                    focus:true,
                    ajax: '@href',
                    autoSize:true,
                    trigger: 'a#popup',
                    ajaxText: ("loading..."),
                    onHide: function(h){
                        h.o.remove(); // remove overlay
                        h.w.fadeOut(688); // hide window
                   }
                });

              
            });

      
           

        </script>
        <SCRIPT LANGUAGE="JavaScript" TYPE="TEXT/JAVASCRIPT">
            function popUp(URL)
            {
                window.open(URL);
            }
            function ShowContent(d) {
                if(d.length < 1) { return; }
                document.getElementById(d).style.display = "block";
            }
            function HideContent(d) {
                if(d.length < 1) { return; }
                document.getElementById(d).style.display = "none";
            }
        </SCRIPT>
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
    </head>
    <body>
        
       	<div id="wrap">
            <div id="title">
                <h1>NiSee</h1>
            </div>
            <div id="side">
                <ul>
                    <li><a class="item" id="popup" href="profile.jsp">Profile</a></li>
                    <li><a class="item" id="popup" href="setting.jsp">Setting</a></li>
                    <li><a class="item" href="logout">Logout</a></li>
                </ul>
            </div>

            <div class="jqmWindow" id="profilepopup"  style="width: 650px; height:500px;overflow: auto;">
                Please wait... <img src="../images/ajax-loader.gif" height="16" width="16" />
            </div>


        
              
            <div id="content">
                <h1>Welcome ${sessionScope.user.userFIRSTNAME} ${sessionScope.user.userFAMILYNAME} </h1>
                   <input class="button" type="submit" value="Show my test history" id="changeset"/>
                <div class="currentcolourset">
                <h2>My test history</h2>
                <table id="historytable">
                    <thead><tr><th>Test No</th><th>Date</th><th>Review</th></tr></thead>
                    <tbody>
                        <c:forEach var="group" items="${sessionScope.user.niseeTestGroupCollection}" varStatus="i">
                            <c:choose>
                                <c:when test="${(i.count) % 2 == 0}">
                                    <tr class="even">
                                    </c:when>
                                    <c:otherwise>
                                    <tr class="odd">
                                    </c:otherwise>
                                </c:choose>

                                <td>${group.testGROUP}</td>
                                <td>${group.testDATE}</td>
                                <td><a href="reviewTest?groupid=${group.testGroupId}" id="popup">Review</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                </div>
                <table>
                    <tr>
                        <td>
                            <input onmouseout="HideContent('quicktip'); return true;" onmouseover="ShowContent('quicktip'); return true;"  type="image" value="Quick Start"  src="images/quickstart.png"   onclick=popUp('getTestInfo?type=1')  />
                        </td>
                        <td>
                            <input  onmouseout="HideContent('newtip'); return true;" onmouseover="ShowContent('newtip'); return true;"  type="image" value="New Test"  src="images/newtest.png"   onclick="popUp('getTestInfo?type=2')"  />
                        </td>
                    </tr>
                    <tr>
                        <td><div id="quicktip">Quickly start new test using stored images </div></td>
                        <td><div id="newtip">Start a new test with images generated right now</div></td>
                    </tr>
                </table>
            </div>
            <div id="bottom">
                <p><a href="mailto:uow321@googlegroups.com">NiSee Developing Group</a></p>
            </div>

        </div>

    </body>
</html>
