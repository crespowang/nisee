<%-- 
    Document   : newtest
    Created on : 03/09/2010, 10:09:05 AM
    Author     : Crespo
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.lang.Integer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="niseeEntity.NiseeUser"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    ArrayList<Integer> image_ids =( ArrayList<Integer> )session.getAttribute("image_ids");
    HashMap<Integer,String> id_answer = (HashMap<Integer,String>)session.getAttribute("map");
    Integer counter = (Integer) session.getAttribute("counter");
    System.out.println("total images: " + image_ids.size() + " current: " + counter);
    String answer = request.getParameter("answer");
    int image_id = -1;
    if(counter > 0){
     
          image_id = image_ids.get(counter - 1);
             System.out.println("image:" + image_id+ "answer:" + answer);

        id_answer.put(new Integer(image_id), answer);
    }
    if(counter < image_ids.size()){
        image_id = image_ids.get(counter);
        counter++;
        session.setAttribute("map", id_answer);
        session.setAttribute("counter", counter);
    }else{
            image_id = -1;
            String finish = "finishTest";
            RequestDispatcher dispatch = request.getRequestDispatcher(finish);
            dispatch.forward(request, response);
    }
   

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
           <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
             <script type="text/javascript" src="scripts/jquery-1.4.2.min.js"></script>
          <script type="text/javascript" src="scripts/validation.js"></script>

                           <script type="text/javascript">

            $(document).ready(function(){
                $("#imageform").validate({
                    rules: {
                        answer:{required:true}
                    }
                });

            });
        </script>

       <script type="text/javascript">
             $(document).ready(function(){
        
                  
          
                    $("div#testimagearea").slideUp(300).delay(1000).fadeIn(400);
                    $("div#testinputarea").slideUp(300).delay(1000).fadeIn(400);
                    $("div#testimagearea").delay(6000).slideUp(500).fadeOut(400);
                    $(".useranswer:first").delay(1000).focus();

   
          });
        </script>

        <title>Test</title>
    </head>
    <body>
         <form action="newtest.jsp" method="POST" id="imageform" >
             <fieldset>
        <table  id="testimagetable" align="center">
         
       
            <tr><td colspan="2">
                     <div id="testimagearea">
        <img alt="" height="300" width="300"  src="getImage?id=<%=image_id%>" />
                         </div>
                 </td></tr>
           
             <tr>
            
                 <td> <p>Please input your answer here</p><input type="text" name="answer" value=""  class="useranswer" /></td>
                 
             </tr>
          

             <tr>
            <td><div id="testinputarea"> <input type="submit" value="Next" /> </div></td>
            
       
         </tr>
         
       
         </table>
             </fieldset>
                  </form>
    </body>
</html>
