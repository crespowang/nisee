<%-- 
    Document   : profile.jsp
    Created on : 04/09/2010, 10:53:28 PM
    Author     : Crespo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />

        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.dataTables.min.js" ></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery-ui-1.8.5.custom.min" ></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.ui.core.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/user_management_datatable.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/validation.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jqModal.js"></script>
        
        <script type="text/javascript">

	$(document).ready(function() {
            $("#datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
                yearRange: '1950:2010',
                autoSize: true,
                changeMonth: true,
		changeYear: true
            });
        });
    </script>
            <script type="text/javascript">

            $(document).ready(function(){
                $("#modifyuser").validate({
                rules: {
	          userFAMILYNAME: {required:true},
	          userFIRSTNAME: {required:true}
                }
                });
            });
        </script>
                <style type="text/css" title="currentStyle">
            @import "css/jquery.ui.all.css";
        </style>
        <title> ${sessionScope.user.userFIRSTNAME} ${sessionScope.user.userFAMILYNAME}</title>
    </head>
    <body>
         <form action="modifyProfile.jsp" id="modifyuser" method="GET" >
                  <fieldset>
        <table id="profiletable">
            <tbody>
              <tr><th>Username</th><td>${sessionScope.user.userUSERNAME}</td></tr>
              <tr><th>Surname</th><td colspan="3">${sessionScope.user.userFAMILYNAME}</td></tr>
                <tr><th>First name</th><td>  ${sessionScope.user.userFIRSTNAME}</td></tr>
                <tr><th>Date of Birth</th><td>${sessionScope.user.userDOB} </td></tr>
                  <tr><th>Gender</th><td>${sessionScope.user.userGENDER}</td></tr>
                  <tr><th>Email</th><td>${sessionScope.user.userEMAIL}</td></tr>
            </tbody>
            <tfoot>
                <tr><th><input type="submit" value="Modify" /></th><td><input type="button" value="Close"  class="jqmClose" /></td></tr>
        
        </tfoot>
        </table>
                      </fieldset>
               
         </form>
    </body>
</html>
