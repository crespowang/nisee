/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
    $("#refresh").click(function(event){
        var params=$('input').serialize();
        $("#img").attr("src",  "images/wait.gif");
        $.ajax( {
            url: "GenerateForeground",
            data: params,
            type: 'get',
            success:update_img
        }
        );
    });

    
    $("#saves").click(function(event){
        $.ajax( {
            url: "SaveShape",
            type: 'get',
            success: alert("Save it!")
        }
        );
    });

    $("#perc-range").slider({
        range: true,
        min: 0,
        max: 100,
        values: [50, 90],
        slide: function(event, ui) {
            var s = ui.values[0];
            var m = eval( ui.values[1]-ui.values[0] );
            var l = eval(100 - ui.values[1]);
            $("#percentage").val('S: ' + s +
                ' M: ' + m +
                ' L: ' + l
                );
            $("#perc1").val(s);
            $("#perc2").val(m);
            $("#perc3").val(l);
        }
    });

    $("#percentage").val('S: ' + $("#perc-range").slider("values", 0) +
        ' M: ' + eval($("#perc-range").slider("values", 1)-$("#perc-range").slider("values", 0)) +
        ' L:' + eval(100 - $("#perc-range").slider("values", 1)));
    var s = $("#perc-range").slider("values", 0);
    var v1 = $("#perc-range").slider("values", 1);
    var m = v1-s;
    var l = 100 - v1;
    $("#perc1").val(s);
    $("#perc2").val(m);
    $("#perc3").val(l);


    $("#size-range > span").each(function() {
        // read initial values from markup and remove that
        var value = parseInt($(this).text());
        $(this).empty().slider({
            value: value,
            range: "min",
            max: 20,
            animate: true,
            slide: function(event, ui) {
                var sizeType = $(this).attr("size");
                //                alert(sizeType + " " + ui.value);
                $(sizeType).val(ui.value);
                $("#size").val('('
                    + $("#size1").val()
                    + ", " + $("#size2").val()
                    + ", " + $("#size3").val() + ")"
                    );
            }
        });
    });
    $("#size").val('('
        + $("#size1").val()
        + ", " + $("#size2").val()
        + ", " + $("#size3").val() + ")"
        );

    $("#colorCombo").dialog({
        autoOpen: false,
        height: 300,
        width: 350,
        modal: true
    });

    $(".button").button();

    $( "#chooseColorCombo" ).button().click(function() {
        $( "#colorCombo" ).dialog( "open" );
    });

    $("#combos tr").click( function(){
        var htmlStr = '<tr><td>ID</td><td>Foreground1</td><td>Foreground2</td><td>Background</td><td></td></tr>';
        $("#choosedColorCombo").html(htmlStr + $(this).html());
    });

    $("#addnewuser").validate({
        rules: {
            familyname: {
                required:true
            },
            firstname: {
                required:true
            }
        }
    });

});
function update_img(){
    $("#img").attr("src",  'GetDemo?'+Math.random());
}