
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NiSee</title>
<link href="niseecss.css" rel="stylesheet" type="text/css" media="screen" />
  <script type="text/javascript" src="scripts/jquery-1.4.2.min.js"></script>
          <script type="text/javascript" src="scripts/validation.js"></script>
      <script type="text/javascript">
              $(document).ready(function(){
                $("#loginform").validate();
            });
        </script>
</head>
<body>
    <div class="loginpage" >
        <h1>Welcome to NiSee</h1>

<div align="center" id="login">
<form name="login" method="post" action="login" id="loginform">
  <label> </label>
  <table >
    <tr>
      <th>Username</th>
      <td> <input type="text" name="usr" id="username" class="required" /></td>
    </tr>
    <tr>
      <th>Password</th>
      <td><input type="password" name="pwd" id="pwd" class="required" /></td>
    </tr>
    <tr>
      <td><input type="submit" value="Login" /></td>
      <td>
          <c:choose>
              <c:when test="${requestScope.error == 'pwd_usr_err'}">
                Incorrect password or username

              </c:when>
          </c:choose>


      </td>
    </tr>
  </table>



</form>

</div>
</div>
</body>
</html>
