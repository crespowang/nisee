/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeServlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import niseeDatabase.niseeDB;
import niseeEntity.NiseeUser;

/**
 *
 * @author Crespo
 */
public class login extends HttpServlet {
     private static final String loginfail = "login.jsp";
    private static final String userCenter = "center.jsp";
    private static final String adminCenter = "Center";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet login</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet login at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String usr = request.getParameter("usr");
        String pwd = request.getParameter("pwd");

        niseeDB db = new niseeDB();
        NiseeUser user = db.login(usr, pwd);
        if (user == null) {
            request.setAttribute("error", "pwd_usr_err");
            RequestDispatcher dispatch = request.getRequestDispatcher(loginfail);
            dispatch.forward(request, response);
            return;
        } else {
            db.cleanUnfinished(user);
            user = db.refreshuser(user);
            db.close();
            if(user.getUserPRIVILEGES() == 0){
                System.out.println("Student user");
                request.getSession().setAttribute("user", user);
                response.sendRedirect(userCenter);
            }
            if(user.getUserPRIVILEGES() == 1){
                System.out.println("Admin user");
                // redirect to admin page
                request.getSession().setAttribute("user", user);
                response.sendRedirect(adminCenter);
            }
                return;
        }
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
