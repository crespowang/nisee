/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niseeServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import niseeDatabase.niseeDB;
import niseeEntity.NiseeTestGroup;
import niseeEntity.NiseeTestSingle;
import niseeEntity.NiseeUser;

/**
 *
 * @author Crespo
 */
public class prepareImage extends HttpServlet {
    private static final String testPage = "newtest.jsp";
    private static String WINDOWCLOSE = "windowclose.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet prepareImage</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet prepareImage at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Should start to generate images");
        // group images in table NiSEE_TEST_GROUP and return groupid
        int testType = Integer.valueOf(request.getParameter("type")).intValue();
        String testtypeid = request.getParameter("testName");
        System.out.println("What's test type?" +testtypeid + " id:" +  testtypeid);
  
        int groupid = 1;
        NiseeUser user = (NiseeUser)request.getSession().getAttribute("user");
        niseeDB db = new niseeDB();
        NiseeTestGroup loadtest = null;
        if (testType == 1) {
            System.out.println("What's test type?" +testtypeid + " id:" +  testtypeid);
            int number = Integer.parseInt(request.getParameter("imagesnumber"));
            loadtest = db.quickStart(user, number,Integer.valueOf(testtypeid));
            groupid = loadtest.getTestGroupId();
            System.out.println("This is a quick start");
        }else{
            //loadtest = new NiseeTestGroup(testType);
            //what is testType?
            System.out.println("testname="+testtypeid);
            loadtest = db.newTest(user, testtypeid);//either pass test type ("alphabetic") or typeID
            groupid = loadtest.getTestGroupId();//?
            System.out.println("This is a new test! Should invoke generator");
        }


        Collection<NiseeTestSingle> niseeTestSingleCollection = loadtest.getNiseeTestSingleCollection();
        ArrayList<Integer> images_ids = new ArrayList<Integer>();

        for (NiseeTestSingle single : niseeTestSingleCollection) {
            images_ids.add(single.getNiseeTestSinglePK().getImageIMAGEID());
        }

        for (Integer s : images_ids) {
            System.out.println("image collections " + s);

        }
        Integer counter = new Integer(0);

        HashMap<Integer, String> id_answer = new HashMap<Integer, String>();
        for (NiseeTestSingle single : niseeTestSingleCollection) {

            String put = id_answer.put(single.getNiseeTestSinglePK().getImageIMAGEID(), null);
        }
        db.close();
        request.getSession().setAttribute("groupid", new Integer(groupid));
        request.getSession().setAttribute("map", id_answer);
        request.getSession().setAttribute("image_ids", images_ids);
        request.getSession().setAttribute("counter", counter);
        RequestDispatcher dispatch = request.getRequestDispatcher(testPage);
        dispatch.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("this is post");
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
