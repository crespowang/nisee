/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niseeServlet;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import niseeDatabase.niseeDB;
import niseeEntity.NiseeImage;

/**
 *
 * @author Crespo
 */
public class getImage extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final String summaryPage = "summary.jsp";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet getImage</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet getImage at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
        } finally {
            out.close();
        }
    }


    private void keepAnswer(int groupid, HashMap<Integer, String> id_answer){
        niseeDB db = new niseeDB();
        db.keepAnswer(id_answer, groupid);
        db.close();
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.valueOf((String) request.getParameter("id"));
//         ArrayList<Integer> image_ids = (ArrayList<Integer>) request.getSession().getAttribute("image_ids");
//         if(!image_ids.contains(id)){
//             System.out.println("Error! Trying to access image that doesn't belong to this test group");
//             return;
//
//         }

        System.out.println("img id " + id);
        response.setContentType("image/jpg");
        niseeDB db = new niseeDB();
        NiseeImage image = db.getImage(id);
        db.close();
        byte[] data = image.getImageIMAGEFILE();
        OutputStream out = response.getOutputStream();
        out.write(data);
        out.close();
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
