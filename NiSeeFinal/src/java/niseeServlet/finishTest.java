/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niseeServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import niseeDatabase.niseeDB;
import niseeDatabase.niseeTempAnswerTable;
import niseeEntity.NiseeAnswer;
import niseeEntity.NiseeImage;


/**
 *
 * @author Crespo
 */
public class finishTest extends HttpServlet {

    private static final String summaryPage = "summary.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet finishTest</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet finishTest at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        NiseeUser user = (NiseeUser) request.getSession().getAttribute("user");
        HashMap<Integer, String> id_answer = (HashMap<Integer, String>) request.getSession().getAttribute("map");
        ArrayList<Integer> image_ids = (ArrayList<Integer>) request.getSession().getAttribute("image_ids");
//        Integer counter = (Integer) request.getSession().getAttribute("counter");
//        Integer groupid = (Integer)request.getSession().getAttribute("groupid");
        niseeDB db = new niseeDB();
        ArrayList<niseeTempAnswerTable> tabs = new ArrayList<niseeTempAnswerTable>();
        for (Integer id : image_ids) {
      
             NiseeImage img = db.getImage(id);

             System.out.println("Going to fetch ans for Image id " + id + img.getImageIMAGETIMESTAMP());
               ArrayList<String> correct_answer = new ArrayList<String>();
               System.out.println("Test shape id " + img.getNiseeTestShape().getTestShapeId());
            for(NiseeAnswer ans :  img.getNiseeTestShape().getNiseeAnswerCollection()){
                correct_answer.add(ans.getAnswer());
            }
            String user_answer = id_answer.get(id);
            System.out.println("Image: " + id + " user answer: " + user_answer + " correct answer: ");
            for(String possible : correct_answer){
                System.out.println("Ans:" + possible);
            }

            niseeTempAnswerTable tab = new niseeTempAnswerTable(user_answer, correct_answer, id);
            tabs.add(tab);
        }
        request.setAttribute("tabs", tabs);



        db.close();

        RequestDispatcher dispatch = request.getRequestDispatcher(summaryPage);
        dispatch.forward(request, response);


    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
