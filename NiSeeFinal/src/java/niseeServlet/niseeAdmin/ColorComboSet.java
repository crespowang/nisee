/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niseeServlet.niseeAdmin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import niseeDatabase.niseeDB;
import niseeEntity.NiseeColourComboSet;

/**
 *
 * @author Bing
 */
public class ColorComboSet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        niseeDB db = new niseeDB();
        List<NiseeColourComboSet> niseeColourComboSets = db.getComboSets();
        ArrayList<NiseeColourComboSet> niseeColourComboSets_no_duplicated = new ArrayList<NiseeColourComboSet>() ;
        int single_set_id = -1;
        ArrayList<Integer> single_set_ids = new ArrayList<Integer>();
        for(NiseeColourComboSet single_set : niseeColourComboSets){


            if(!single_set_ids.contains(single_set.getColourCombosetId())){
                    niseeColourComboSets_no_duplicated.add(single_set);
            }
            single_set_id = single_set.getColourCombosetId();
            single_set_ids.add(single_set_id);
        }

        request.setAttribute("niseeColourComboSets", niseeColourComboSets_no_duplicated);
        db.close();

        RequestDispatcher dispatch = request.getRequestDispatcher("admin/colorComboSet.jsp");
        dispatch.forward(request, response);

        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ColorComboSet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ColorComboSet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
