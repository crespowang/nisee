/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niseeServlet.niseeAdmin;

import niseeEntity.NiseeUser;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Bing
 */
public class AddAdmin extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String familyName = request.getParameter("faname");
        String firstName = request.getParameter("finame");
        String password = request.getParameter("password");
        String user = request.getParameter("user");
        String email = request.getParameter("email");
        String priviledeg = request.getParameter("plg");
        Date userDOB = null;
        String userGENDER = null;
        String userPASSWORD = null;
        int userPRIVILEGES = 0;

        if (familyName != null && firstName != null
                && password != null && user != null && email != null
                && priviledeg != null) {
            int plg = Integer.parseInt(priviledeg);
            //NiseeUser admin = new NiseeUser(user, familyName, firstName, plg, password, email);


//            NiseeUser admin = new NiseeUser(user, familyName, firstName, userDOB, userGENDER, userPASSWORD, email, userPRIVILEGES);
            NiseeUser admin = null;
            UserController userController = new UserController();
            if (userController.addAdmin(admin)) {
            } else {
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";

    }// </editor-fold>
}
