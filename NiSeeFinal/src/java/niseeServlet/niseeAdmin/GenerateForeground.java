/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niseeServlet.niseeAdmin;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import niseeDatabase.niseeDB;
import niseeImageGenerator.ImageGenerator;
import niseeImageGenerator.TimeConstrainedImageGenerator;
import niseeUtility.uServlet;

/**
 *
 * @author Bing
 */
public class GenerateForeground extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String path = uServlet.RealContextPath(request);
        String imagePath = request.getParameter("imageSrc");
        if (imagePath == null) {
            imagePath = "1.jpg";
        }
        
        File file = new File(path + imagePath);
        System.out.println("Path: " + path);
        System.out.println("imagePath: " + imagePath);
        System.out.println(file);


//        int colourset[][][] = {{{130, 170, 150}, {249, 128, 93}}, {{183, 180, 128}, {236, 170, 141}}, {{138, 129, 125}, {226, 137, 128}}};

        TimeConstrainedImageGenerator gen = new TimeConstrainedImageGenerator();//ImageGenerator gen = new ImageGenerator();
        //Administrator could do something like this:
        ArrayList<Short> percentages = new ArrayList();
        percentages.add(Short.parseShort(request.getParameter("perc1")));
        percentages.add(Short.parseShort(request.getParameter("perc2")));
        percentages.add(Short.parseShort(request.getParameter("perc3")));

        ArrayList<Short> sizes = new ArrayList();
        sizes.add(Short.parseShort(request.getParameter("size1")));
        sizes.add(Short.parseShort(request.getParameter("size2")));
        sizes.add(Short.parseShort(request.getParameter("size3")));

        String str = (String) request.getParameter("testTypeID");

        int tesTypeId = 1;
        if ( str!=null ) {
            tesTypeId = Integer.parseInt(str);
        }

        String colorComboStr = request.getParameter("colorComboID");
        int colorComboID = Integer.parseInt(colorComboStr);
        niseeDB db = new niseeDB();
        List<Color> colors = db.getColourComboByID(colorComboID);

        String answer = request.getParameter("answer");
        //FileOutputStream ot = new FileOutputStream(path+name+".jpg");


        Short format = 2;
        String value = request.getParameter("format");
        if (value != null) {
            format = Short.parseShort(value);
        }

        Short gap = 1;
        value = request.getParameter("gap");
        if (value != null) {
            gap = Short.parseShort(value);
        }

        int numberOfForegrounds = 2;

        HttpSession session = request.getSession(true);
        try{
            if (gen.FillForeground(file, format, percentages, sizes, gap, numberOfForegrounds)) {
                BufferedImage bf = gen.CreateImage(gen.getForeground1(), gen.getForeground2(), format, percentages, sizes, gap, colors.get(0), colors.get(1), colors.get(2));
                //BufferedImage bf = gen.CreateImage(foreground1, null, format, percentages, sizes, gap, new Color(colourset[x][1][0], colourset[x][1][1], colourset[x][1][2]),null, new Color(colourset[x][0][0], colourset[x][0][1], colourset[x][0][2]));
                session.setAttribute("demo", bf);
                session.setAttribute("name", "Bing");
                session.setAttribute("fore1", gen.getForeground1());
                session.setAttribute("fore2", gen.getForeground2());
                session.setAttribute("percs", percentages);
                session.setAttribute("sizes", sizes);
                session.setAttribute("format", format);
                session.setAttribute("gapsize", gap);
                session.setAttribute("answer", answer);
                session.setAttribute("ttypeid", tesTypeId);
                System.out.println("It's Ok");
            } else {
                System.out.println("I've no idea");
            }
        }catch(TimeoutException e){
            BufferedImage bf = new BufferedImage(309, 309, BufferedImage.TYPE_INT_RGB);
            for (int x = 0; x < 309; x++) {
                for (int y = 0; y < 309; y++) {
                    bf.setRGB(x, y, Color.white.getRGB());
                }
            }
            session.setAttribute("demo", bf);
        }catch(InterruptedException e){
            BufferedImage bf = new BufferedImage(309, 309, BufferedImage.TYPE_INT_RGB);
            for (int x = 0; x < 309; x++) {
                for (int y = 0; y < 309; y++) {
                    bf.setRGB(x, y, Color.white.getRGB());
                }
            }
            session.setAttribute("demo", bf);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
