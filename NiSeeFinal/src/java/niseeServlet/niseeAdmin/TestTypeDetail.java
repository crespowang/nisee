/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niseeServlet.niseeAdmin;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import niseeDatabase.niseeDB;
import niseeEntity.NiseeColourComboSet;
import niseeEntity.NiseeTestType;
import niseeUtility.ColourCombo;
import niseeUtility.ImageFile;
import niseeUtility.TestType;
import niseeUtility.uServlet;

/**
 *
 * @author Bing
 */
public class TestTypeDetail extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        niseeDB db = new niseeDB();
        int testID = -1;
        int save = -1;
        if(request.getParameter("testID") != null){
            testID = Integer.parseInt(request.getParameter("testID"));
        }
        if(request.getParameter("save") !=null){
        save = Integer.parseInt(request.getParameter("save"));
        }
        System.out.println("save" + save);
        if (save == 1) {
            int colourset = Integer.parseInt(request.getParameter("colourset"));
            System.out.println("setting test with testID" + testID + " 's coloursetID" + colourset);
            db.setTestTypesColourSet(testID, colourset);
        }

        TestType testType = db.getTestTypeByID(testID);
        request.setAttribute("testType", testType);
        List<NiseeColourComboSet> niseeColourComboSets = db.getComboSets();
        ArrayList<NiseeColourComboSet> niseeColourComboSets_no_duplicated = new ArrayList<NiseeColourComboSet>();
        int single_set_id = -1;

        ArrayList<Integer> single_set_ids = new ArrayList<Integer>();
        for (NiseeColourComboSet single_set : niseeColourComboSets) {
            if (!single_set_ids.contains(single_set.getColourCombosetId())) {
                niseeColourComboSets_no_duplicated.add(single_set);
            }
            single_set_id = single_set.getColourCombosetId();
            single_set_ids.add(single_set_id);
        }


        db.close();

        String imagePath = uServlet.RealContextPath(request);

        imagePath += "/imagebase/" + testID;

        File file = new File(imagePath);

//        System.out.println(file.toString());

        File[] fileArray = file.listFiles();

        List<ImageFile> imageFiles = new ArrayList<ImageFile>();

        List<File> files = null;

        if (fileArray != null) {
            files = Arrays.asList(file.listFiles());
            for (File f : files) {
                imageFiles.add(new ImageFile(f.getName(), testID));
            }
        }
        System.out.println("Curr id " + testType.getColorComboSetID());

        request.setAttribute("currentsetid", testType.getColorComboSetID());
        request.setAttribute("coloursets", niseeColourComboSets_no_duplicated);
        request.setAttribute("imageFiles", imageFiles);
        request.setAttribute("imageSrc", "imagebase/");

//        getServletConfig().getServletContext().getRequestDispatcher(
//                "/admin/browserimgs.jsp").forward(request, response);


        RequestDispatcher dispatch = request.getRequestDispatcher("admin/testTypeDetail.jsp");
        dispatch.forward(request, response);


        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ColorComboSetDetail</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ColorComboSetDetail at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
