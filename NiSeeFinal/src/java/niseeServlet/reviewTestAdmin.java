/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import niseeDatabase.niseeDB;
import niseeDatabase.niseeTempAnswerTable;
import niseeEntity.NiseeAnswer;
import niseeEntity.NiseeTestGroup;
import niseeEntity.NiseeTestSingle;
import niseeEntity.NiseeUser;

/**
 *
 * @author Crespo
 */
public class reviewTestAdmin extends HttpServlet {
      private static String reviewPage = "review.jsp";
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet reviewTestAdmin</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet reviewTestAdmin at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         Integer groupid  = Integer.valueOf(request.getParameter("groupid"));
        niseeDB db = new niseeDB();
 
            String username = request.getParameter("username");
            System.out.println("REVIEW FROM ADMIN USERNAME " + username);
             NiseeUser user = db.getUser(username);

        String testdate = "unset";
         for(NiseeTestGroup gp :  user.getNiseeTestGroupCollection()){
            if(groupid.compareTo(gp.getTestGroupId()) == 0){
                ArrayList<niseeTempAnswerTable> tabs = new ArrayList<niseeTempAnswerTable>();
                testdate = gp.getTestDATE();
              for(NiseeTestSingle single : gp.getNiseeTestSingleCollection()){
                    Integer image_id = single.getNiseeTestSinglePK().getImageIMAGEID();
                    String user_ans = single.getUserAnswer();

                    ArrayList<String> correct_ans = new ArrayList<String>();
                    for(NiseeAnswer ans :  single.getNiseeImage().getNiseeTestShape().getNiseeAnswerCollection()){
                        correct_ans.add(ans.getAnswer());

                    }

                    niseeTempAnswerTable tab = new niseeTempAnswerTable(user_ans, correct_ans, image_id);
                    tabs.add(tab);
              }
                db.close();
                request.setAttribute("testdate", testdate);
                request.setAttribute("tabs",tabs);
                RequestDispatcher dispatch = request.getRequestDispatcher(reviewPage);
                dispatch.forward(request, response);
                break;
            }
         }

    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
