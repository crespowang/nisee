/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niseeServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import niseeDatabase.niseeDB;
import niseeEntity.NiseeTestShape;
import niseeEntity.NiseeTestType;

/**
 *
 * @author stussy
 */
public class getTestInfo extends HttpServlet {
    private static final String QUICKSTART = "choosequickstart.jsp";
    private static final String NEWTEST="chooseNewTest.jsp";
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet getTestInfo</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet getTestInfo at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int type = Integer.valueOf(request.getParameter("type")).intValue();
        ArrayList<TypeNumName> type_names = new ArrayList<TypeNumName>();
        niseeDB db = new niseeDB();
        List<NiseeTestType> testtypes = (List) db.getNiseeTestTypes();
        if(testtypes != null && type == 1){

          for(NiseeTestType testtype : testtypes){
              int count = 0;
              for(NiseeTestShape shape : testtype.getNiseeTestShapeCollection()){
                    if (shape != null) {
                        count += shape.getNiseeImageCollection().size();
                    }

              }
              if(count > 0){
                  type_names.add(new TypeNumName(testtype.getName(), count,testtype.getTypeId()));
              }

          }
            request.setAttribute("type", type);
            request.setAttribute("typenames", type_names);
            RequestDispatcher dispatch = request.getRequestDispatcher(QUICKSTART);
            dispatch.forward(request, response);

        }

       if(testtypes != null && type == 2){
                for (NiseeTestType test : testtypes) {
                    int numberOfColourCombinations = db.getComboset(test.getColourCombosetId()).size();
                    if ((numberOfColourCombinations > 0) && (test.getNiseeTestShapeCollection().size()>0)) {
                       type_names.add(new TypeNumName(test.getName(), numberOfColourCombinations,test.getTypeId()));
                      }
                }
            request.setAttribute("type", type);
            request.setAttribute("typenames", type_names);
            RequestDispatcher dispatch = request.getRequestDispatcher(NEWTEST);
            dispatch.forward(request, response);

       }


    }

    public class TypeNumName{

        public TypeNumName(String name,int num,int id) {
            this.name = name;
            this.num = num;
            this.type_id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }
        
        private String name;
        private int num;
        private int type_id;

        public int getType_id() {
            return type_id;
        }

        public void setType_id(int type_id) {
            this.type_id = type_id;
        }
        
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("GETTESTINFO POST");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
