package niseeImageGenerator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.Serializable;
//import testgenerator.*;

/**
 *
 * @author stussy
 */
public class niCircle extends niShape implements Serializable {

    private niSquare tempNiSquare;
    private int SMALLEST_CIRCLE = 3;
    /*
     * the x and y values of a niCircle refer to the top left corner coordinates, and the size value is twice the radius
     */

    public niCircle(int tx, int ty, int tsize) {
        super(tx, ty, tsize);
        tempNiSquare = new niSquare(tx, ty, tsize);
    }

    //sometimes checks negative values ?
    public boolean checkCircle(int whitespace, BufferedImage bufferedImage) {
        if (x < 0 || y < 0) {
            return false;
        }

        if (size < SMALLEST_CIRCLE) {
            return tempNiSquare.checkSquare(whitespace, bufferedImage);
        }

        for (int i = x - whitespace; i < x + size + whitespace; i++) {//for (int i = (x - size) - whitespace; i < x + whitespace; i++) {
            for (int j = y - whitespace; j < y + size + whitespace; j++) {//for (int j = (y - size) - whitespace; j < y + whitespace; j++) {
                if (i >= 0 && j >= 0) {
                    if (i >= bufferedImage.getWidth() || j >= bufferedImage.getHeight()) {
                        return false;
                    }
                    if (bufferedImage.getRGB(i, j) != Color.white.getRGB()) {
                        //check if it is in circle
                        Double radius = Math.sqrt(Math.pow(Math.abs((size/2)-i), 2) + Math.pow(Math.abs((size/2)-j), 2));//2 Math.sqrt(Math.pow(Math.abs(i - x), 2) + Math.pow(Math.abs(j - y), 2));
                        if ((radius + whitespace) >= (size / 2))//1 if((Math.sqrt(Math.pow(Math.abs((x+(size/2))-i),2)+Math.pow(Math.abs((y+(size/2))-j),2))+whitespace)<size)
                        {
                            return false;
                        }
                        //if((Math.sqrt(Math.pow(Math.abs((x+(size/2))-i),2)+Math.pow(Math.abs((y+(size/2))-j),2))+whitespace)<size)

                    }
                }
            }
        }
        return true;
    }

    public boolean checkDrawCircle(int whitespace, BufferedImage bufferedImage, Graphics2D g2, Color theColour, int randomFactor) {
        if (size < SMALLEST_CIRCLE) {
            return tempNiSquare.checkDrawSquare(whitespace, bufferedImage, g2, theColour, randomFactor);
        }

        colour = theColour;
        boolean check = checkCircle(whitespace, bufferedImage);
        if (check) {
            drawCircle(g2, randomFactor);
            return true;
        }
        return false;
    }

    public void drawCircle(Graphics2D g2, int randomFactor) {
        if (size < SMALLEST_CIRCLE) {
            tempNiSquare.drawSquare(g2, randomFactor);
            return;
        }

        if (randomFactor == 0) {
            g2.setColor(colour);
        } else if (randomFactor < 0) {
            g2.setColor(colour.darker());
        } else {
            g2.setColor(colour.brighter());
        }
        g2.fillOval(x, y, size, size);
        //g2.fillOval(x - (size/2), y - (size/2), size, size);
        //g2.fillOval(x - size, y - size, size, size);


        //g2.fillArc(x, y, size*2, size*2, 0, 360);//
        //g2.setColor(colour.BLUE);
        //g2.fillOval(x - size, y - size, 2, 2);//g2.fillArc(x, y, size*2, size*2, 0, 360);//
    }

    public void setColour(Color tcolour) {
        colour = tcolour;
        if (size < SMALLEST_CIRCLE) {
            tempNiSquare.setColour(tcolour);
            return;
        }
    }
}
