/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niseeImageGenerator;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.*;
import java.util.concurrent.ExecutorService;

/**
 *
 * @author stussy
 */
public class TimeConstrainedImageGenerator {

    int FOREGROUND_TIME_BEFORE_TIMEOUT = 25;
    TimeUnit FOREGROUND_TIME_UNIT = TimeUnit.SECONDS;
    int BACKGROUND_TIME_BEFORE_TIMEOUT = 25;
    TimeUnit BACKGROUND_TIME_UNIT = TimeUnit.SECONDS;

    ExecutorService executor = Executors.newFixedThreadPool(1);
    ImageGenerator generator = new ImageGenerator();
    BufferedImage img;

    public boolean FillForeground(final File objectf, final short format, final ArrayList<Short> percentages, final ArrayList<Short> sizes, final short gapSize, final int numberOfForegrounds) throws InterruptedException, TimeoutException {
        FutureTask<String> future = new FutureTask<String>(
                new Callable<String>() {

                    public String call() {
                        if (generator.FillForeground(objectf, format, percentages, sizes, gapSize, numberOfForegrounds)) {
                            return "true";
                        } else {
                            return "false";
                        }
                    }
                });
        executor.execute(future);

        try {
            if (future.get(FOREGROUND_TIME_BEFORE_TIMEOUT, FOREGROUND_TIME_UNIT).equalsIgnoreCase("true")) {
                return true;
            } else {
                return false;
            }
        } catch (ExecutionException e) {
        }
        executor.shutdown();
        return false;
    }

    public BufferedImage CreateImage(final ArrayList<niShape> foreground1, final ArrayList<niShape> foreground2, final short format, final ArrayList<Short> percentages, final ArrayList<Short> sizes, final short gapSize, final Color foregroundColour1, final Color foregroundColour2, final Color backgroundColour) throws InterruptedException,TimeoutException {
        //CreateImage(foreground1, foreground2, format, percentages, sizes, gapSize, foregroundColour1, foregroundColour2, backgroundColour);
        FutureTask<String> future = new FutureTask<String>(
                new Callable<String>() {

                    public String call() {
                        img = generator.CreateImage(foreground1, foreground2, format, percentages, sizes, gapSize, foregroundColour1, foregroundColour2, backgroundColour);
                        if (img != null) {
                            return "true";
                        } else {
                            return "false";
                        }
                    }
                });
        executor.execute(future);

        try {
            future.get(BACKGROUND_TIME_BEFORE_TIMEOUT, BACKGROUND_TIME_UNIT);
            return img;
        } catch (ExecutionException e) {
        }
        executor.shutdown();
        return null;

    }

    public ArrayList<niShape> getForeground1() {
        return generator.foreground1;
    }

    public ArrayList<niShape> getForeground2() {
        return generator.foreground2;
    }
}
