package niseeImageGenerator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.io.Serializable;
//import testgenerator.*;
//import java.util.Random;

/**
 *
 * @author stussy
 */
public class niSquare extends niShape implements Serializable{

    public niSquare(int tx, int ty, int tsize) {
        super(tx, ty, tsize);
    }

    public boolean checkSquare(int whitespace, BufferedImage bufferedImage) {
        if (x < 0 || y < 0) {
            return false;
        }
            
        for (int i = x - whitespace; i < x + size + whitespace; i++) {
            for (int j = y - whitespace; j < y + size + whitespace; j++) {
                if (i >= 0 && j >= 0) {
                    if (i >= bufferedImage.getWidth() || j >= bufferedImage.getHeight()) {
                        return false;
                    }
                    if (bufferedImage.getRGB(i, j) != Color.white.getRGB()) {
                        return false;
                    }
                }
            }
        }
        return true;

    }

    public boolean checkDrawSquare(int whitespace, BufferedImage bufferedImage, Graphics2D g2, Color theColour, int randomFactor) {
        //int MinSize = 5;
        colour = theColour;
        boolean check = checkSquare(whitespace, bufferedImage);
        if (check)
        {
            drawSquare(g2, randomFactor);
            return true;
        }
        else
            return false;
        //boolean check = checkSquare(whitespace, bufferedImage);
        /*while (!check && size > MinSize) {
            size -= 2;
            check = checkSquare(whitespace, bufferedImage);
        }
        if (check) {
            drawSquare(g2);
            return true;
        }
        return false;*/
    }

    public void drawSquare(Graphics2D g2, int randomFactor) {
        if (randomFactor==0)
        g2.setColor(colour);
        else if(randomFactor<0)
            g2.setColor(colour.darker());
        else
            g2.setColor(colour.brighter());
        g2.fillRect(x, y, size, size);
        //g2.setColor(colour.GREEN);
        //g2.fillRect(x, y, 2, 2);//g2.fillArc(x, y, size*2, size*2, 0, 360);//
    }

    /*public void drawSquare(BufferedImage image) {
    Graphics2D g2 = (Graphics2D) image.createGraphics();
    for(int i=0;i<tcount-1;i++)
    mySquares[i].drawSquare(g2);
    }*/
    public void setColour(Color tcolour) {
        colour = tcolour;
    }
}

