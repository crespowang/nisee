/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Bing
 */
@Entity
@Table(name = "nisee_shape_coor")
@NamedQueries({
    @NamedQuery(name = "NiseeShapeCoor.findAll", query = "SELECT n FROM NiseeShapeCoor n"),
    @NamedQuery(name = "NiseeShapeCoor.findByCoorX", query = "SELECT n FROM NiseeShapeCoor n WHERE n.coorX = :coorX"),
    @NamedQuery(name = "NiseeShapeCoor.findByCoorY", query = "SELECT n FROM NiseeShapeCoor n WHERE n.coorY = :coorY"),
    @NamedQuery(name = "NiseeShapeCoor.findByScale", query = "SELECT n FROM NiseeShapeCoor n WHERE n.scale = :scale"),
    @NamedQuery(name = "NiseeShapeCoor.findById", query = "SELECT n FROM NiseeShapeCoor n WHERE n.id = :id")})
public class NiseeShapeCoor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "COOR_X")
    private int coorX;
    @Basic(optional = false)
    @Column(name = "COOR_Y")
    private int coorY;
    @Basic(optional = false)
    @Column(name = "SCALE")
    private int scale;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "shape_id", referencedColumnName = "shape_ID")
    @ManyToOne(optional = false)
    private NiseeShape niseeShape;

    public NiseeShapeCoor() {
    }

    public NiseeShapeCoor(Integer id) {
        this.id = id;
    }

    public NiseeShapeCoor(Integer id, int coorX, int coorY, int scale) {
        this.id = id;
        this.coorX = coorX;
        this.coorY = coorY;
        this.scale = scale;
    }

    public int getCoorX() {
        return coorX;
    }

    public void setCoorX(int coorX) {
        this.coorX = coorX;
    }

    public int getCoorY() {
        return coorY;
    }

    public void setCoorY(int coorY) {
        this.coorY = coorY;
    }

    public int getScale() {
        return scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public NiseeShape getNiseeShape() {
        return niseeShape;
    }

    public void setNiseeShape(NiseeShape niseeShape) {
        this.niseeShape = niseeShape;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeShapeCoor)) {
            return false;
        }
        NiseeShapeCoor other = (NiseeShapeCoor) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeShapeCoor[id=" + id + "]";
    }

}
