/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Bing
 */
@Entity
@Table(name = "nisee_user")
@NamedQueries({
    @NamedQuery(name = "NiseeUser.findAll", query = "SELECT n FROM NiseeUser n"),
    @NamedQuery(name = "NiseeUser.findByUserUSERNAME", query = "SELECT n FROM NiseeUser n WHERE n.userUSERNAME = :userUSERNAME"),
    @NamedQuery(name = "NiseeUser.findByUserFAMILYNAME", query = "SELECT n FROM NiseeUser n WHERE n.userFAMILYNAME = :userFAMILYNAME"),
    @NamedQuery(name = "NiseeUser.findByUserFIRSTNAME", query = "SELECT n FROM NiseeUser n WHERE n.userFIRSTNAME = :userFIRSTNAME"),
    @NamedQuery(name = "NiseeUser.findByUserDOB", query = "SELECT n FROM NiseeUser n WHERE n.userDOB = :userDOB"),
    @NamedQuery(name = "NiseeUser.findByUserGENDER", query = "SELECT n FROM NiseeUser n WHERE n.userGENDER = :userGENDER"),
    @NamedQuery(name = "NiseeUser.findByUserPASSWORD", query = "SELECT n FROM NiseeUser n WHERE n.userPASSWORD = :userPASSWORD"),
    @NamedQuery(name = "NiseeUser.findByUserPASSWORDandUSERNAME", query = "SELECT n FROM NiseeUser n WHERE n.userUSERNAME = :userUSERNAME AND n.userPASSWORD = :userPASSWORD"),
    @NamedQuery(name = "NiseeUser.findByUserPASSWORDandUSERNAMEandPRIVILEGES", query = "SELECT n FROM NiseeUser n WHERE n.userPRIVILEGES = :userPRIVILEGES AND n.userPASSWORD = :userPASSWORD AND n.userUSERNAME = :userUSERNAME"),
    @NamedQuery(name = "NiseeUser.findByUserEMAIL", query = "SELECT n FROM NiseeUser n WHERE n.userEMAIL = :userEMAIL"),
    @NamedQuery(name = "NiseeUser.findByUserPRIVILEGES", query = "SELECT n FROM NiseeUser n WHERE n.userPRIVILEGES = :userPRIVILEGES")})
public class NiseeUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "user_USERNAME")
    private String userUSERNAME;
    @Basic(optional = false)
    @Column(name = "user_FAMILY_NAME")
    private String userFAMILYNAME;
    @Basic(optional = false)
    @Column(name = "user_FIRST_NAME")
    private String userFIRSTNAME;
    @Basic(optional = false)
    @Column(name = "user_DOB")
    @Temporal(TemporalType.DATE)
    private Date userDOB;
    @Basic(optional = false)
    @Column(name = "user_GENDER")
    private String userGENDER;
    @Basic(optional = false)
    @Column(name = "user_PASSWORD")
    private String userPASSWORD;
    @Basic(optional = false)
    @Column(name = "user_EMAIL")
    private String userEMAIL;
    @Basic(optional = false)
    @Column(name = "user_PRIVILEGES")
    private short userPRIVILEGES;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "niseeUser")
    private Collection<NiseeTestGroup> niseeTestGroupCollection;

    public NiseeUser() {
    }

    public NiseeUser(String userUSERNAME) {
        this.userUSERNAME = userUSERNAME;
    }

    public NiseeUser(String userUSERNAME, String userFAMILYNAME, String userFIRSTNAME, Date userDOB, String userGENDER, String userPASSWORD, String userEMAIL, short userPRIVILEGES) {
        this.userUSERNAME = userUSERNAME;
        this.userFAMILYNAME = userFAMILYNAME;
        this.userFIRSTNAME = userFIRSTNAME;
        this.userDOB = userDOB;
        this.userGENDER = userGENDER;
        this.userPASSWORD = userPASSWORD;
        this.userEMAIL = userEMAIL;
        this.userPRIVILEGES = userPRIVILEGES;
    }

    public String getUserUSERNAME() {
        return userUSERNAME;
    }

    public void setUserUSERNAME(String userUSERNAME) {
        this.userUSERNAME = userUSERNAME;
    }

    public String getUserFAMILYNAME() {
        return userFAMILYNAME;
    }

    public void setUserFAMILYNAME(String userFAMILYNAME) {
        this.userFAMILYNAME = userFAMILYNAME;
    }

    public String getUserFIRSTNAME() {
        return userFIRSTNAME;
    }

    public void setUserFIRSTNAME(String userFIRSTNAME) {
        this.userFIRSTNAME = userFIRSTNAME;
    }

    public String getUserDOB() {
         SimpleDateFormat sdf =
          new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(userDOB);
    }

    public void setUserDOB(Date userDOB) {
        this.userDOB = userDOB;
    }

    public String getUserGENDER() {
        return userGENDER;
    }

    public void setUserGENDER(String userGENDER) {
        this.userGENDER = userGENDER;
    }

    public String getUserPASSWORD() {
        return userPASSWORD;
    }

    public void setUserPASSWORD(String userPASSWORD) {
        this.userPASSWORD = userPASSWORD;
    }

    public String getUserEMAIL() {
        return userEMAIL;
    }

    public void setUserEMAIL(String userEMAIL) {
        this.userEMAIL = userEMAIL;
    }

    public short getUserPRIVILEGES() {
        return userPRIVILEGES;
    }

    public void setUserPRIVILEGES(short userPRIVILEGES) {
        this.userPRIVILEGES = userPRIVILEGES;
    }

    public Collection<NiseeTestGroup> getNiseeTestGroupCollection() {
        return niseeTestGroupCollection;
    }

    public void setNiseeTestGroupCollection(Collection<NiseeTestGroup> niseeTestGroupCollection) {
        this.niseeTestGroupCollection = niseeTestGroupCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userUSERNAME != null ? userUSERNAME.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeUser)) {
            return false;
        }
        NiseeUser other = (NiseeUser) object;
        if ((this.userUSERNAME == null && other.userUSERNAME != null) || (this.userUSERNAME != null && !this.userUSERNAME.equals(other.userUSERNAME))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeUser[userUSERNAME=" + userUSERNAME + "]";
    }

}
