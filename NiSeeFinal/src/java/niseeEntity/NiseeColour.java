/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Bing
 */
@Entity
@Table(name = "nisee_colour")
@NamedQueries({
    @NamedQuery(name = "NiseeColour.findAll", query = "SELECT n FROM NiseeColour n"),
    @NamedQuery(name = "NiseeColour.findByColourId", query = "SELECT n FROM NiseeColour n WHERE n.colourId = :colourId"),
    @NamedQuery(name = "NiseeColour.findByRed", query = "SELECT n FROM NiseeColour n WHERE n.red = :red"),
    @NamedQuery(name = "NiseeColour.findByGreen", query = "SELECT n FROM NiseeColour n WHERE n.green = :green"),
    @NamedQuery(name = "NiseeColour.findByBlue", query = "SELECT n FROM NiseeColour n WHERE n.blue = :blue")})
public class NiseeColour implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "colour_id")
    private Integer colourId;
    @Basic(optional = false)
    @Column(name = "red")
    private short red;
    @Basic(optional = false)
    @Column(name = "green")
    private short green;
    @Basic(optional = false)
    @Column(name = "blue")
    private short blue;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "niseeColour")
    private Collection<NiseeColourCombo> niseeColourComboCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "niseeColour1")
    private Collection<NiseeColourCombo> niseeColourComboCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "niseeColour2")
    private Collection<NiseeColourCombo> niseeColourComboCollection2;

    public NiseeColour() {
    }

    public NiseeColour(Integer colourId) {
        this.colourId = colourId;
    }

    public NiseeColour(Integer colourId, short red, short green, short blue) {
        this.colourId = colourId;
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public Integer getColourId() {
        return colourId;
    }

    public void setColourId(Integer colourId) {
        this.colourId = colourId;
    }

    public short getRed() {
        return red;
    }

    public void setRed(short red) {
        this.red = red;
    }

    public short getGreen() {
        return green;
    }

    public void setGreen(short green) {
        this.green = green;
    }

    public short getBlue() {
        return blue;
    }

    public void setBlue(short blue) {
        this.blue = blue;
    }

    public Collection<NiseeColourCombo> getNiseeColourComboCollection() {
        return niseeColourComboCollection;
    }

    public void setNiseeColourComboCollection(Collection<NiseeColourCombo> niseeColourComboCollection) {
        this.niseeColourComboCollection = niseeColourComboCollection;
    }

    public Collection<NiseeColourCombo> getNiseeColourComboCollection1() {
        return niseeColourComboCollection1;
    }

    public void setNiseeColourComboCollection1(Collection<NiseeColourCombo> niseeColourComboCollection1) {
        this.niseeColourComboCollection1 = niseeColourComboCollection1;
    }

    public Collection<NiseeColourCombo> getNiseeColourComboCollection2() {
        return niseeColourComboCollection2;
    }

    public void setNiseeColourComboCollection2(Collection<NiseeColourCombo> niseeColourComboCollection2) {
        this.niseeColourComboCollection2 = niseeColourComboCollection2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (colourId != null ? colourId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeColour)) {
            return false;
        }
        NiseeColour other = (NiseeColour) object;
        if ((this.colourId == null && other.colourId != null) || (this.colourId != null && !this.colourId.equals(other.colourId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeColour[colourId=" + colourId + "]";
    }

}
