/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Bing
 */
@Entity
@Table(name = "nisee_format_gapsize")
@NamedQueries({
    @NamedQuery(name = "NiseeFormatGapsize.findAll", query = "SELECT n FROM NiseeFormatGapsize n"),
    @NamedQuery(name = "NiseeFormatGapsize.findByFormat", query = "SELECT n FROM NiseeFormatGapsize n WHERE n.format = :format"),
    @NamedQuery(name = "NiseeFormatGapsize.findByGapsize", query = "SELECT n FROM NiseeFormatGapsize n WHERE n.gapsize = :gapsize"),
    @NamedQuery(name = "NiseeFormatGapsize.findByTestShapeId", query = "SELECT n FROM NiseeFormatGapsize n WHERE n.testShapeId = :testShapeId")})
public class NiseeFormatGapsize implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "format")
    private short format;
    @Basic(optional = false)
    @Column(name = "gapsize")
    private short gapsize;
    @Id
    @Basic(optional = false)
    @Column(name = "test_shape_id")
    private Integer testShapeId;
    @JoinColumn(name = "test_shape_id", referencedColumnName = "test_shape_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private NiseeTestShape niseeTestShape;

    public NiseeFormatGapsize() {
    }

    public NiseeFormatGapsize(Integer testShapeId) {
        this.testShapeId = testShapeId;
    }

    public NiseeFormatGapsize(Integer testShapeId, short format, short gapsize) {
        this.testShapeId = testShapeId;
        this.format = format;
        this.gapsize = gapsize;
    }

    public short getFormat() {
        return format;
    }

    public void setFormat(short format) {
        this.format = format;
    }

    public short getGapsize() {
        return gapsize;
    }

    public void setGapsize(short gapsize) {
        this.gapsize = gapsize;
    }

    public Integer getTestShapeId() {
        return testShapeId;
    }

    public void setTestShapeId(Integer testShapeId) {
        this.testShapeId = testShapeId;
    }

    public NiseeTestShape getNiseeTestShape() {
        return niseeTestShape;
    }

    public void setNiseeTestShape(NiseeTestShape niseeTestShape) {
        this.niseeTestShape = niseeTestShape;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (testShapeId != null ? testShapeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeFormatGapsize)) {
            return false;
        }
        NiseeFormatGapsize other = (NiseeFormatGapsize) object;
        if ((this.testShapeId == null && other.testShapeId != null) || (this.testShapeId != null && !this.testShapeId.equals(other.testShapeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeFormatGapsize[testShapeId=" + testShapeId + "]";
    }

}
