/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Bing
 */
@Entity
@Table(name = "nisee_answer")
@NamedQueries({
    @NamedQuery(name = "NiseeAnswer.findAll", query = "SELECT n FROM NiseeAnswer n"),
    @NamedQuery(name = "NiseeAnswer.findById", query = "SELECT n FROM NiseeAnswer n WHERE n.id = :id"),
    @NamedQuery(name = "NiseeAnswer.findByAnswer", query = "SELECT n FROM NiseeAnswer n WHERE n.answer = :answer")})
public class NiseeAnswer implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "answer")
    private String answer;
    @JoinColumn(name = "test_shape_id", referencedColumnName = "test_shape_id")
    @ManyToOne(optional = false)
    private NiseeTestShape niseeTestShape;

    public NiseeAnswer() {
    }

    public NiseeAnswer(Integer id) {
        this.id = id;
    }

    public NiseeAnswer(Integer id, String answer) {
        this.id = id;
        this.answer = answer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public NiseeTestShape getNiseeTestShape() {
        return niseeTestShape;
    }

    public void setNiseeTestShape(NiseeTestShape niseeTestShape) {
        this.niseeTestShape = niseeTestShape;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeAnswer)) {
            return false;
        }
        NiseeAnswer other = (NiseeAnswer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeAnswer[id=" + id + "]";
    }

}
