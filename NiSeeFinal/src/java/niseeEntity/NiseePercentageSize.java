/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Bing
 */
@Entity
@Table(name = "nisee_percentage_size")
@NamedQueries({
    @NamedQuery(name = "NiseePercentageSize.findAll", query = "SELECT n FROM NiseePercentageSize n"),
    @NamedQuery(name = "NiseePercentageSize.findByPercentages", query = "SELECT n FROM NiseePercentageSize n WHERE n.percentages = :percentages"),
    @NamedQuery(name = "NiseePercentageSize.findBySize", query = "SELECT n FROM NiseePercentageSize n WHERE n.size = :size"),
    @NamedQuery(name = "NiseePercentageSize.findById", query = "SELECT n FROM NiseePercentageSize n WHERE n.id = :id")})
public class NiseePercentageSize implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "percentages")
    private short percentages;
    @Basic(optional = false)
    @Column(name = "size")
    private short size;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "test_shape_id", referencedColumnName = "test_shape_id")
    @ManyToOne(optional = false)
    private NiseeTestShape niseeTestShape;

    public NiseePercentageSize() {
    }

    public NiseePercentageSize(Integer id) {
        this.id = id;
    }

    public NiseePercentageSize(Integer id, short percentages, short size) {
        this.id = id;
        this.percentages = percentages;
        this.size = size;
    }

    public short getPercentages() {
        return percentages;
    }

    public void setPercentages(short percentages) {
        this.percentages = percentages;
    }

    public short getSize() {
        return size;
    }

    public void setSize(short size) {
        this.size = size;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public NiseeTestShape getNiseeTestShape() {
        return niseeTestShape;
    }

    public void setNiseeTestShape(NiseeTestShape niseeTestShape) {
        this.niseeTestShape = niseeTestShape;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseePercentageSize)) {
            return false;
        }
        NiseePercentageSize other = (NiseePercentageSize) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseePercentageSize[id=" + id + "]";
    }

}
