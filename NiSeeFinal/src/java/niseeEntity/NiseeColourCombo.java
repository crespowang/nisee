/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Bing
 */
@Entity
@Table(name = "nisee_colour_combo")
@NamedQueries({
    @NamedQuery(name = "NiseeColourCombo.findAll", query = "SELECT n FROM NiseeColourCombo n"),
    @NamedQuery(name = "NiseeColourCombo.findByColourComboId", query = "SELECT n FROM NiseeColourCombo n WHERE n.colourComboId = :colourComboId"),
    @NamedQuery(name = "NiseeColourCombo.findByColourDiagnosis", query = "SELECT n FROM NiseeColourCombo n WHERE n.colourDiagnosis = :colourDiagnosis")})
public class NiseeColourCombo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "colour_combo_id")
    private Integer colourComboId;
    @Column(name = "colour_diagnosis")
    private String colourDiagnosis;
    @JoinColumn(name = "colour_fore_b", referencedColumnName = "colour_id")
    @ManyToOne(optional = false)
    private NiseeColour niseeColour;
    @JoinColumn(name = "colour_fore_a", referencedColumnName = "colour_id")
    @ManyToOne(optional = false)
    private NiseeColour niseeColour1;
    @JoinColumn(name = "colour_back", referencedColumnName = "colour_id")
    @ManyToOne(optional = false)
    private NiseeColour niseeColour2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "niseeColourCombo")
    private Collection<NiseeColourComboSet> niseeColourComboSetCollection;

    public NiseeColourCombo() {
    }

    public NiseeColourCombo(Integer colourComboId) {
        this.colourComboId = colourComboId;
    }

    public Integer getColourComboId() {
        return colourComboId;
    }

    public void setColourComboId(Integer colourComboId) {
        this.colourComboId = colourComboId;
    }

    public String getColourDiagnosis() {
        return colourDiagnosis;
    }

    public void setColourDiagnosis(String colourDiagnosis) {
        this.colourDiagnosis = colourDiagnosis;
    }

    public NiseeColour getNiseeColour() {
        return niseeColour;
    }

    public void setNiseeColour(NiseeColour niseeColour) {
        this.niseeColour = niseeColour;
    }

    public NiseeColour getNiseeColour1() {
        return niseeColour1;
    }

    public void setNiseeColour1(NiseeColour niseeColour1) {
        this.niseeColour1 = niseeColour1;
    }

    public NiseeColour getNiseeColour2() {
        return niseeColour2;
    }

    public void setNiseeColour2(NiseeColour niseeColour2) {
        this.niseeColour2 = niseeColour2;
    }

    public Collection<NiseeColourComboSet> getNiseeColourComboSetCollection() {
        return niseeColourComboSetCollection;
    }

    public void setNiseeColourComboSetCollection(Collection<NiseeColourComboSet> niseeColourComboSetCollection) {
        this.niseeColourComboSetCollection = niseeColourComboSetCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (colourComboId != null ? colourComboId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeColourCombo)) {
            return false;
        }
        NiseeColourCombo other = (NiseeColourCombo) object;
        if ((this.colourComboId == null && other.colourComboId != null) || (this.colourComboId != null && !this.colourComboId.equals(other.colourComboId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeColourCombo[colourComboId=" + colourComboId + "]";
    }

}
