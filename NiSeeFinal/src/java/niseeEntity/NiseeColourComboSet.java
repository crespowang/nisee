/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niseeEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Bing
 */
@Entity
@Table(name = "nisee_colour_combo_set")
@NamedQueries({
    @NamedQuery(name = "NiseeColourComboSet.findAll", query = "SELECT n FROM NiseeColourComboSet n"),
    @NamedQuery(name = "NiseeColourComboSet.findAllDistinctName", query = "SELECT DISTINCT n.name FROM NiseeColourComboSet n"),
    @NamedQuery(name = "NiseeColourComboSet.findByColourCombosetId", query = "SELECT n FROM NiseeColourComboSet n WHERE n.colourCombosetId = :colourCombosetId"),
    @NamedQuery(name = "NiseeColourComboSet.findAllDistinctName", query = "SELECT DISTINCT n.name FROM NiseeColourComboSet n"),
    @NamedQuery(name = "NiseeColourComboSet.findNameByColourCombosetId", query = "SELECT DISTINCT n.name FROM NiseeColourComboSet n WHERE n.colourCombosetId = :colourCombosetId"),
    @NamedQuery(name = "NiseeColourComboSet.findById", query = "SELECT n FROM NiseeColourComboSet n WHERE n.id = :id"),
    @NamedQuery(name = "NiseeColourComboSet.findByName", query = "SELECT n FROM NiseeColourComboSet n WHERE n.name = :name")})
    
public class NiseeColourComboSet implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "colour_comboset_id")
    private int colourCombosetId;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @JoinColumn(name = "colour_combo_id", referencedColumnName = "colour_combo_id")
    @ManyToOne(optional = false)
    private NiseeColourCombo niseeColourCombo;

    public NiseeColourComboSet() {
    }

    public NiseeColourComboSet(Integer id) {
        this.id = id;
    }

    public NiseeColourComboSet(Integer id, int colourCombosetId) {
        this.id = id;
        this.colourCombosetId = colourCombosetId;
    }

    public int getColourCombosetId() {
        return colourCombosetId;
    }

    public void setColourCombosetId(int colourCombosetId) {
        this.colourCombosetId = colourCombosetId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NiseeColourCombo getNiseeColourCombo() {
        return niseeColourCombo;
    }

    public void setNiseeColourCombo(NiseeColourCombo niseeColourCombo) {
        this.niseeColourCombo = niseeColourCombo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeColourComboSet)) {
            return false;
        }
        NiseeColourComboSet other = (NiseeColourComboSet) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeColourComboSet[id=" + id + "]";
    }
}
