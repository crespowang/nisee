/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Bing
 */
@Entity
@Table(name = "nisee_test_shape")
@NamedQueries({
    @NamedQuery(name = "NiseeTestShape.findAll", query = "SELECT n FROM NiseeTestShape n"),
    @NamedQuery(name = "NiseeTestShape.findByTestShapeId", query = "SELECT n FROM NiseeTestShape n WHERE n.testShapeId = :testShapeId")})
public class NiseeTestShape implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "test_shape_id")
    private Integer testShapeId;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "niseeTestShape")
    private NiseeFormatGapsize niseeFormatGapsize;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "niseeTestShape")
    private Collection<NiseePercentageSize> niseePercentageSizeCollection;
    @JoinColumn(name = "type_id", referencedColumnName = "type_id")
    @ManyToOne(optional = false)
    private NiseeTestType niseeTestType;
    @JoinColumn(name = "foreground_b_shape_id", referencedColumnName = "shape_ID")
    @ManyToOne(optional = false)
    private NiseeShape niseeShape;
    @JoinColumn(name = "foreground_a_shape_id", referencedColumnName = "shape_ID")
    @ManyToOne(optional = false)
    private NiseeShape niseeShape1;
    @OneToMany(mappedBy = "niseeTestShape")
    private Collection<NiseeImage> niseeImageCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "niseeTestShape")
    private Collection<NiseeAnswer> niseeAnswerCollection;

    public NiseeTestShape() {
    }

    public NiseeTestShape(Integer testShapeId) {
        this.testShapeId = testShapeId;
    }

    public Integer getTestShapeId() {
        return testShapeId;
    }

    public void setTestShapeId(Integer testShapeId) {
        this.testShapeId = testShapeId;
    }

    public NiseeFormatGapsize getNiseeFormatGapsize() {
        return niseeFormatGapsize;
    }

    public void setNiseeFormatGapsize(NiseeFormatGapsize niseeFormatGapsize) {
        this.niseeFormatGapsize = niseeFormatGapsize;
    }

    public Collection<NiseePercentageSize> getNiseePercentageSizeCollection() {
        return niseePercentageSizeCollection;
    }

    public void setNiseePercentageSizeCollection(Collection<NiseePercentageSize> niseePercentageSizeCollection) {
        this.niseePercentageSizeCollection = niseePercentageSizeCollection;
    }

    public NiseeTestType getNiseeTestType() {
        return niseeTestType;
    }

    public void setNiseeTestType(NiseeTestType niseeTestType) {
        this.niseeTestType = niseeTestType;
    }

    public NiseeShape getNiseeShape() {
        return niseeShape;
    }

    public void setNiseeShape(NiseeShape niseeShape) {
        this.niseeShape = niseeShape;
    }

    public NiseeShape getNiseeShape1() {
        return niseeShape1;
    }

    public void setNiseeShape1(NiseeShape niseeShape1) {
        this.niseeShape1 = niseeShape1;
    }

    public Collection<NiseeImage> getNiseeImageCollection() {
        return niseeImageCollection;
    }

    public void setNiseeImageCollection(Collection<NiseeImage> niseeImageCollection) {
        this.niseeImageCollection = niseeImageCollection;
    }

    public Collection<NiseeAnswer> getNiseeAnswerCollection() {
        return niseeAnswerCollection;
    }

    public void setNiseeAnswerCollection(Collection<NiseeAnswer> niseeAnswerCollection) {
        this.niseeAnswerCollection = niseeAnswerCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (testShapeId != null ? testShapeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeTestShape)) {
            return false;
        }
        NiseeTestShape other = (NiseeTestShape) object;
        if ((this.testShapeId == null && other.testShapeId != null) || (this.testShapeId != null && !this.testShapeId.equals(other.testShapeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeTestShape[testShapeId=" + testShapeId + "]";
    }

}
