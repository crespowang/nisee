/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Bing
 */
@Entity
@Table(name = "nisee_shape")
@NamedQueries({
    @NamedQuery(name = "NiseeShape.findAll", query = "SELECT n FROM NiseeShape n"),
    @NamedQuery(name = "NiseeShape.findByShapeID", query = "SELECT n FROM NiseeShape n WHERE n.shapeID = :shapeID"),
    @NamedQuery(name = "NiseeShape.findByAnswer", query = "SELECT n FROM NiseeShape n WHERE n.answer = :answer")})
public class NiseeShape implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "shape_ID")
    private Integer shapeID;
    @Column(name = "answer")
    private String answer;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "niseeShape")
    private Collection<NiseeShapeCoor> niseeShapeCoorCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "niseeShape")
    private Collection<NiseeTestShape> niseeTestShapeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "niseeShape1")
    private Collection<NiseeTestShape> niseeTestShapeCollection1;

    public NiseeShape() {
    }

    public NiseeShape(Integer shapeID) {
        this.shapeID = shapeID;
    }

    public Integer getShapeID() {
        return shapeID;
    }

    public void setShapeID(Integer shapeID) {
        this.shapeID = shapeID;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Collection<NiseeShapeCoor> getNiseeShapeCoorCollection() {
        return niseeShapeCoorCollection;
    }

    public void setNiseeShapeCoorCollection(Collection<NiseeShapeCoor> niseeShapeCoorCollection) {
        this.niseeShapeCoorCollection = niseeShapeCoorCollection;
    }

    public Collection<NiseeTestShape> getNiseeTestShapeCollection() {
        return niseeTestShapeCollection;
    }

    public void setNiseeTestShapeCollection(Collection<NiseeTestShape> niseeTestShapeCollection) {
        this.niseeTestShapeCollection = niseeTestShapeCollection;
    }

    public Collection<NiseeTestShape> getNiseeTestShapeCollection1() {
        return niseeTestShapeCollection1;
    }

    public void setNiseeTestShapeCollection1(Collection<NiseeTestShape> niseeTestShapeCollection1) {
        this.niseeTestShapeCollection1 = niseeTestShapeCollection1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (shapeID != null ? shapeID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeShape)) {
            return false;
        }
        NiseeShape other = (NiseeShape) object;
        if ((this.shapeID == null && other.shapeID != null) || (this.shapeID != null && !this.shapeID.equals(other.shapeID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeShape[shapeID=" + shapeID + "]";
    }

}
