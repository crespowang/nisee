/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Bing
 */
@Entity
@Table(name = "nisee_test_single")
@NamedQueries({
    @NamedQuery(name = "NiseeTestSingle.findAll", query = "SELECT n FROM NiseeTestSingle n"),
    @NamedQuery(name = "NiseeTestSingle.findByImageIMAGEID", query = "SELECT n FROM NiseeTestSingle n WHERE n.niseeTestSinglePK.imageIMAGEID = :imageIMAGEID"),
    @NamedQuery(name = "NiseeTestSingle.findByTestGROUPID", query = "SELECT n FROM NiseeTestSingle n WHERE n.niseeTestSinglePK.testGROUPID = :testGROUPID"),
    @NamedQuery(name = "NiseeTestSingle.findByUserAnswer", query = "SELECT n FROM NiseeTestSingle n WHERE n.userAnswer = :userAnswer")})
public class NiseeTestSingle implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NiseeTestSinglePK niseeTestSinglePK;
    @Column(name = "user_answer")
    private String userAnswer;
    @JoinColumn(name = "image_IMAGE_ID", referencedColumnName = "image_IMAGE_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private NiseeImage niseeImage;
    @JoinColumn(name = "test_GROUP_ID", referencedColumnName = "TEST_GROUP_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private NiseeTestGroup niseeTestGroup;

    public NiseeTestSingle() {
    }

    public NiseeTestSingle(NiseeTestSinglePK niseeTestSinglePK) {
        this.niseeTestSinglePK = niseeTestSinglePK;
    }

    public NiseeTestSingle(int imageIMAGEID, int testGROUPID) {
        this.niseeTestSinglePK = new NiseeTestSinglePK(imageIMAGEID, testGROUPID);
    }

    public NiseeTestSinglePK getNiseeTestSinglePK() {
        return niseeTestSinglePK;
    }

    public void setNiseeTestSinglePK(NiseeTestSinglePK niseeTestSinglePK) {
        this.niseeTestSinglePK = niseeTestSinglePK;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public NiseeImage getNiseeImage() {
        return niseeImage;
    }

    public void setNiseeImage(NiseeImage niseeImage) {
        this.niseeImage = niseeImage;
    }

    public NiseeTestGroup getNiseeTestGroup() {
        return niseeTestGroup;
    }

    public void setNiseeTestGroup(NiseeTestGroup niseeTestGroup) {
        this.niseeTestGroup = niseeTestGroup;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (niseeTestSinglePK != null ? niseeTestSinglePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeTestSingle)) {
            return false;
        }
        NiseeTestSingle other = (NiseeTestSingle) object;
        if ((this.niseeTestSinglePK == null && other.niseeTestSinglePK != null) || (this.niseeTestSinglePK != null && !this.niseeTestSinglePK.equals(other.niseeTestSinglePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeTestSingle[niseeTestSinglePK=" + niseeTestSinglePK + "]";
    }

}
