/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Bing
 */
@Entity
@Table(name = "nisee_test_type")
@NamedQueries({
    @NamedQuery(name = "NiseeTestType.findAll", query = "SELECT n FROM NiseeTestType n"),
    @NamedQuery(name = "NiseeTestType.findByTypeId", query = "SELECT n FROM NiseeTestType n WHERE n.typeId = :typeId"),
    @NamedQuery(name = "NiseeTestType.findByName", query = "SELECT n FROM NiseeTestType n WHERE n.name = :name"),
    @NamedQuery(name = "NiseeTestType.findByColourCombosetId", query = "SELECT n FROM NiseeTestType n WHERE n.colourCombosetId = :colourCombosetId")})
public class NiseeTestType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "type_id")
    private Integer typeId;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "colour_comboset_id")
    private int colourCombosetId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "niseeTestType")
    private Collection<NiseeTestShape> niseeTestShapeCollection;

    public NiseeTestType() {
    }

    public NiseeTestType(Integer typeId) {
        this.typeId = typeId;
    }

    public NiseeTestType(Integer typeId, String name, int colourCombosetId) {
        this.typeId = typeId;
        this.name = name;
        this.colourCombosetId = colourCombosetId;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getColourCombosetId() {
        return colourCombosetId;
    }

    public void setColourCombosetId(int colourCombosetId) {
        this.colourCombosetId = colourCombosetId;
    }

    public Collection<NiseeTestShape> getNiseeTestShapeCollection() {
        return niseeTestShapeCollection;
    }

    public void setNiseeTestShapeCollection(Collection<NiseeTestShape> niseeTestShapeCollection) {
        this.niseeTestShapeCollection = niseeTestShapeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (typeId != null ? typeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeTestType)) {
            return false;
        }
        NiseeTestType other = (NiseeTestType) object;
        if ((this.typeId == null && other.typeId != null) || (this.typeId != null && !this.typeId.equals(other.typeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeTestType[typeId=" + typeId + "]";
    }

}
