/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niseeUtility;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bing
 */
public class uServlet {

    public static String imagebase = "imagebase/";

    public static String RealContextPath(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        ServletContext context = session.getServletContext();
        String contextPath = context.getRealPath(request.getContextPath());
        int index = contextPath.lastIndexOf('\\');
        int lastIndex = index >= 0 ? index : contextPath.lastIndexOf('/');
        if (lastIndex >= 0) {
            return contextPath.substring(0, lastIndex)+"/";
        } else {
            return "";
        }
    }

    public static String RealImagePath(HttpServletRequest request) {
        return RealContextPath(request) + "/" + imagebase;
    }
}
