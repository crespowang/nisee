/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeUtility;


/**
 *
 * @author Bing
 */
public class ColourCombo {
    String foreground1;
    String foreground2;
    String background;
    int comboSetId;
    int id;
    int idAndSetId;

    public int getIdAndSetId() {
        return idAndSetId;
    }
    
    public ColourCombo(int id, String fore1, String fore2, String back, int setId) {
        this.id = id;
        this.foreground1 = fore1;
        this.foreground2 = fore2;
        this.background = back;
        this.comboSetId = setId;
        this.idAndSetId = 0;
    }

    public ColourCombo(int id, String fore1, String fore2, String back, int setId, int idAndSetId) {
        this.id = id;
        this.foreground1 = fore1;
        this.foreground2 = fore2;
        this.background = back;
        this.comboSetId = setId;
        this.idAndSetId = idAndSetId;
    }

    public int getId() {
        return id;
    }
    
    public String getBackground() {
        return background;
    }

    public String getForeground1() {
        return foreground1;
    }

    public String getForeground2() {
        return foreground2;
    }

    public String toTxt() {
        return "id: " + comboSetId + "\tbackground: " + background + "\tfroe1: " + foreground1 + "\tfroe2: " + foreground2;
    }

    public int getComboSetId() {
        return comboSetId;
    }
}