/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niseeUtility;

/**
 *
 * @author Bing
 */
public class TestType {

    int id;
    int colorComboSetID;
    String name;
    String colorComboSetName;

    public TestType() {
        
    }

    public TestType(int id, int colorComboSetID, String name) {
        this.id = id;
        this.colorComboSetID = colorComboSetID;
        this.name = name;
    }

    public TestType(int id, int colorComboSetID, String name, String colorComboSetName) {
        this.id = id;
        this.colorComboSetID = colorComboSetID;
        this.name = name;
        this.colorComboSetName = colorComboSetName;
    }

    public int getColorComboSetID() {
        return colorComboSetID;
    }

    public String getColorComboSetName() {
        return colorComboSetName;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
