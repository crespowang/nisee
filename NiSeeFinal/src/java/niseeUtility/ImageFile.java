/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeUtility;

/**
 *
 * @author Bing
 */
public class ImageFile {
    String name;
    String path;
    String srcPath;

    public ImageFile(String name, int path) {
        this.name = name;
        this.path = path+"/"+name;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }
}
