/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niseeDatabase;

import java.util.ArrayList;

/**
 *
 * @author Crespo
 */
public class niseeTempAnswerTable {

    public ArrayList<String> getCorrect_ans() {
        return correct_ans;
    }

    public int getImage_id() {
        return image_id;
    }

    public String getUser_ans() {
        return user_ans;
    }

    public boolean getCompare(){
        for(String s : correct_ans){
            
            if(s.compareToIgnoreCase(user_ans) == 0){
                compare = true;
                break;
            }
        }

        return compare;
    }

    private String user_ans;
    private ArrayList<String> correct_ans;
    private int image_id;
    private boolean compare;
    public niseeTempAnswerTable(String user,  ArrayList<String> correct, int id) {
        user_ans = user;
        correct_ans = correct;
        image_id = id;
        compare = false;
    }


}
