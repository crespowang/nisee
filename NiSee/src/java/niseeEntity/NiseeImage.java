/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Crespo
 */
@Entity
@Table(name = "nisee_image")
@NamedQueries({
    @NamedQuery(name = "NiseeImage.findAll", query = "SELECT n FROM NiseeImage n"),
    @NamedQuery(name = "NiseeImage.findByImageIMAGEID", query = "SELECT n FROM NiseeImage n WHERE n.imageIMAGEID = :imageIMAGEID"),
    @NamedQuery(name = "NiseeImage.findByImageIMAGETIMESTAMP", query = "SELECT n FROM NiseeImage n WHERE n.imageIMAGETIMESTAMP = :imageIMAGETIMESTAMP")})
public class NiseeImage implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "image_IMAGE_ID")
    private Integer imageIMAGEID;
    @Basic(optional = false)
    @Column(name = "image_IMAGE_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date imageIMAGETIMESTAMP;
    @Basic(optional = false)
    @Lob
    @Column(name = "image_IMAGE_FILE")
    private byte[] imageIMAGEFILE;

    public NiseeImage() {
    }

    public NiseeImage(Integer imageIMAGEID) {
        this.imageIMAGEID = imageIMAGEID;
    }

    public NiseeImage(Integer imageIMAGEID, Date imageIMAGETIMESTAMP, byte[] imageIMAGEFILE) {
        this.imageIMAGEID = imageIMAGEID;
        this.imageIMAGETIMESTAMP = imageIMAGETIMESTAMP;
        this.imageIMAGEFILE = imageIMAGEFILE;
    }

    public Integer getImageIMAGEID() {
        return imageIMAGEID;
    }

    public void setImageIMAGEID(Integer imageIMAGEID) {
        this.imageIMAGEID = imageIMAGEID;
    }

    public Date getImageIMAGETIMESTAMP() {
        return imageIMAGETIMESTAMP;
    }

    public void setImageIMAGETIMESTAMP(Date imageIMAGETIMESTAMP) {
        this.imageIMAGETIMESTAMP = imageIMAGETIMESTAMP;
    }

    public byte[] getImageIMAGEFILE() {
        return imageIMAGEFILE;
    }

    public void setImageIMAGEFILE(byte[] imageIMAGEFILE) {
        this.imageIMAGEFILE = imageIMAGEFILE;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (imageIMAGEID != null ? imageIMAGEID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeImage)) {
            return false;
        }
        NiseeImage other = (NiseeImage) object;
        if ((this.imageIMAGEID == null && other.imageIMAGEID != null) || (this.imageIMAGEID != null && !this.imageIMAGEID.equals(other.imageIMAGEID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeImage[imageIMAGEID=" + imageIMAGEID + "]";
    }

}
