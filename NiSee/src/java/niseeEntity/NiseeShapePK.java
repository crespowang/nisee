/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Crespo
 */
@Embeddable
public class NiseeShapePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "shape_COOR_X")
    private int shapeCOORX;
    @Basic(optional = false)
    @Column(name = "shape_COOR_Y")
    private int shapeCOORY;
    @Basic(optional = false)
    @Column(name = "shape_IMAGE_ID")
    private int shapeIMAGEID;

    public NiseeShapePK() {
    }

    public NiseeShapePK(int shapeCOORX, int shapeCOORY, int shapeIMAGEID) {
        this.shapeCOORX = shapeCOORX;
        this.shapeCOORY = shapeCOORY;
        this.shapeIMAGEID = shapeIMAGEID;
    }

    public int getShapeCOORX() {
        return shapeCOORX;
    }

    public void setShapeCOORX(int shapeCOORX) {
        this.shapeCOORX = shapeCOORX;
    }

    public int getShapeCOORY() {
        return shapeCOORY;
    }

    public void setShapeCOORY(int shapeCOORY) {
        this.shapeCOORY = shapeCOORY;
    }

    public int getShapeIMAGEID() {
        return shapeIMAGEID;
    }

    public void setShapeIMAGEID(int shapeIMAGEID) {
        this.shapeIMAGEID = shapeIMAGEID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) shapeCOORX;
        hash += (int) shapeCOORY;
        hash += (int) shapeIMAGEID;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeShapePK)) {
            return false;
        }
        NiseeShapePK other = (NiseeShapePK) object;
        if (this.shapeCOORX != other.shapeCOORX) {
            return false;
        }
        if (this.shapeCOORY != other.shapeCOORY) {
            return false;
        }
        if (this.shapeIMAGEID != other.shapeIMAGEID) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeShapePK[shapeCOORX=" + shapeCOORX + ", shapeCOORY=" + shapeCOORY + ", shapeIMAGEID=" + shapeIMAGEID + "]";
    }

}
