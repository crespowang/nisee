/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Crespo
 */
@Embeddable
public class NiseeAnswerPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "image_IMAGE_ID")
    private int imageIMAGEID;
    @Basic(optional = false)
    @Column(name = "image_TYPE")
    private String imageTYPE;

    public NiseeAnswerPK() {
    }

    public NiseeAnswerPK(int imageIMAGEID, String imageTYPE) {
        this.imageIMAGEID = imageIMAGEID;
        this.imageTYPE = imageTYPE;
    }

    public int getImageIMAGEID() {
        return imageIMAGEID;
    }

    public void setImageIMAGEID(int imageIMAGEID) {
        this.imageIMAGEID = imageIMAGEID;
    }

    public String getImageTYPE() {
        return imageTYPE;
    }

    public void setImageTYPE(String imageTYPE) {
        this.imageTYPE = imageTYPE;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) imageIMAGEID;
        hash += (imageTYPE != null ? imageTYPE.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeAnswerPK)) {
            return false;
        }
        NiseeAnswerPK other = (NiseeAnswerPK) object;
        if (this.imageIMAGEID != other.imageIMAGEID) {
            return false;
        }
        if ((this.imageTYPE == null && other.imageTYPE != null) || (this.imageTYPE != null && !this.imageTYPE.equals(other.imageTYPE))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeAnswerPK[imageIMAGEID=" + imageIMAGEID + ", imageTYPE=" + imageTYPE + "]";
    }

}
