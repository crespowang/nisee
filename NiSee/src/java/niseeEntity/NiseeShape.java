/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Crespo
 */
@Entity
@Table(name = "nisee_shape")
@NamedQueries({
    @NamedQuery(name = "NiseeShape.findAll", query = "SELECT n FROM NiseeShape n"),
    @NamedQuery(name = "NiseeShape.findByShapeCOORX", query = "SELECT n FROM NiseeShape n WHERE n.niseeShapePK.shapeCOORX = :shapeCOORX"),
    @NamedQuery(name = "NiseeShape.findByShapeCOORY", query = "SELECT n FROM NiseeShape n WHERE n.niseeShapePK.shapeCOORY = :shapeCOORY"),
    @NamedQuery(name = "NiseeShape.findByShapeIMAGEID", query = "SELECT n FROM NiseeShape n WHERE n.niseeShapePK.shapeIMAGEID = :shapeIMAGEID"),
    @NamedQuery(name = "NiseeShape.findByShapeTYPE", query = "SELECT n FROM NiseeShape n WHERE n.shapeTYPE = :shapeTYPE"),
    @NamedQuery(name = "NiseeShape.findByShapeSCALE", query = "SELECT n FROM NiseeShape n WHERE n.shapeSCALE = :shapeSCALE")})
public class NiseeShape implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NiseeShapePK niseeShapePK;
    @Basic(optional = false)
    @Column(name = "shape_TYPE")
    private String shapeTYPE;
    @Basic(optional = false)
    @Column(name = "shape_SCALE")
    private int shapeSCALE;
    @JoinColumn(name = "shape_IMAGE_ID", referencedColumnName = "image_IMAGE_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private NiseeImage niseeImage;

    public NiseeShape() {
    }

    public NiseeShape(NiseeShapePK niseeShapePK) {
        this.niseeShapePK = niseeShapePK;
    }

    public NiseeShape(NiseeShapePK niseeShapePK, String shapeTYPE, int shapeSCALE) {
        this.niseeShapePK = niseeShapePK;
        this.shapeTYPE = shapeTYPE;
        this.shapeSCALE = shapeSCALE;
    }

    public NiseeShape(int shapeCOORX, int shapeCOORY, int shapeIMAGEID) {
        this.niseeShapePK = new NiseeShapePK(shapeCOORX, shapeCOORY, shapeIMAGEID);
    }

    public NiseeShapePK getNiseeShapePK() {
        return niseeShapePK;
    }

    public void setNiseeShapePK(NiseeShapePK niseeShapePK) {
        this.niseeShapePK = niseeShapePK;
    }

    public String getShapeTYPE() {
        return shapeTYPE;
    }

    public void setShapeTYPE(String shapeTYPE) {
        this.shapeTYPE = shapeTYPE;
    }

    public int getShapeSCALE() {
        return shapeSCALE;
    }

    public void setShapeSCALE(int shapeSCALE) {
        this.shapeSCALE = shapeSCALE;
    }

    public NiseeImage getNiseeImage() {
        return niseeImage;
    }

    public void setNiseeImage(NiseeImage niseeImage) {
        this.niseeImage = niseeImage;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (niseeShapePK != null ? niseeShapePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeShape)) {
            return false;
        }
        NiseeShape other = (NiseeShape) object;
        if ((this.niseeShapePK == null && other.niseeShapePK != null) || (this.niseeShapePK != null && !this.niseeShapePK.equals(other.niseeShapePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeShape[niseeShapePK=" + niseeShapePK + "]";
    }

}
