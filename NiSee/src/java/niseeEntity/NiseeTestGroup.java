/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Crespo
 */
@Entity
@Table(name = "nisee_test_group")
@NamedQueries({
    @NamedQuery(name = "NiseeTestGroup.findAll", query = "SELECT n FROM NiseeTestGroup n"),
    @NamedQuery(name = "NiseeTestGroup.findByTestGROUPID", query = "SELECT n FROM NiseeTestGroup n WHERE n.testGROUPID = :testGROUPID"),
    @NamedQuery(name = "NiseeTestGroup.findByTestDIAGNOSIS", query = "SELECT n FROM NiseeTestGroup n WHERE n.testDIAGNOSIS = :testDIAGNOSIS"),
    @NamedQuery(name = "NiseeTestGroup.findByTestDATE", query = "SELECT n FROM NiseeTestGroup n WHERE n.testDATE = :testDATE"),
    @NamedQuery(name = "NiseeTestGroup.findByTestGROUP", query = "SELECT n FROM NiseeTestGroup n WHERE n.testGROUP = :testGROUP")})
public class NiseeTestGroup implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "test_GROUP_ID")
    private Integer testGROUPID;
    @Basic(optional = false)
    @Column(name = "test_DIAGNOSIS")
    private String testDIAGNOSIS;
    @Basic(optional = false)
    @Column(name = "test_DATE")
    @Temporal(TemporalType.DATE)
    private Date testDATE;
    @Basic(optional = false)
    @Column(name = "test_GROUP")
    private int testGROUP;
    @JoinColumn(name = "user_USERNAME", referencedColumnName = "user_USERNAME")
    @ManyToOne(optional = false)
    private NiseeUser niseeUser;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "niseeTestGroup")
    private Collection<NiseeTestSingle> niseeTestSingleCollection;

    public NiseeTestGroup() {
    }

    public NiseeTestGroup(Integer testGROUPID) {
        this.testGROUPID = testGROUPID;
    }

    public NiseeTestGroup(Integer testGROUPID, String testDIAGNOSIS, Date testDATE, int testGROUP) {
        this.testGROUPID = testGROUPID;
        this.testDIAGNOSIS = testDIAGNOSIS;
        this.testDATE = testDATE;
        this.testGROUP = testGROUP;
    }

    public Integer getTestGROUPID() {
        return testGROUPID;
    }

    public void setTestGROUPID(Integer testGROUPID) {
        this.testGROUPID = testGROUPID;
    }

    public String getTestDIAGNOSIS() {
        return testDIAGNOSIS;
    }

    public void setTestDIAGNOSIS(String testDIAGNOSIS) {
        this.testDIAGNOSIS = testDIAGNOSIS;
    }

    public String getTestDATE() {
        SimpleDateFormat sdf =
          new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(testDATE);

    }

    public void setTestDATE(Date testDATE) {
        this.testDATE = testDATE;
    }

    public int getTestGROUP() {


        return testGROUP;
    }

    public void setTestGROUP(int testGROUP) {
        this.testGROUP = testGROUP;
    }

    public NiseeUser getNiseeUser() {
        return niseeUser;
    }

    public void setNiseeUser(NiseeUser niseeUser) {
        this.niseeUser = niseeUser;
    }

    public Collection<NiseeTestSingle> getNiseeTestSingleCollection() {
        return niseeTestSingleCollection;
    }

    public void setNiseeTestSingleCollection(Collection<NiseeTestSingle> niseeTestSingleCollection) {
        this.niseeTestSingleCollection = niseeTestSingleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (testGROUPID != null ? testGROUPID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeTestGroup)) {
            return false;
        }
        NiseeTestGroup other = (NiseeTestGroup) object;
        if ((this.testGROUPID == null && other.testGROUPID != null) || (this.testGROUPID != null && !this.testGROUPID.equals(other.testGROUPID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeTestGroup[testGROUPID=" + testGROUPID + "]";
    }

}
