/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Crespo
 */
@Entity
@Table(name = "nisee_answer")
@NamedQueries({
    @NamedQuery(name = "NiseeAnswer.findAll", query = "SELECT n FROM NiseeAnswer n"),
    @NamedQuery(name = "NiseeAnswer.findByImageIMAGEID", query = "SELECT n FROM NiseeAnswer n WHERE n.niseeAnswerPK.imageIMAGEID = :imageIMAGEID"),
    @NamedQuery(name = "NiseeAnswer.findByImageIMAGEIDandTYPE", query = "SELECT n FROM NiseeAnswer n WHERE n.niseeAnswerPK.imageIMAGEID = :imageIMAGEID AND n.niseeAnswerPK.imageTYPE = :imageTYPE"),
    @NamedQuery(name = "NiseeAnswer.findByImageANSWER", query = "SELECT n FROM NiseeAnswer n WHERE n.imageANSWER = :imageANSWER"),
    @NamedQuery(name = "NiseeAnswer.findByImageTYPE", query = "SELECT n FROM NiseeAnswer n WHERE n.niseeAnswerPK.imageTYPE = :imageTYPE")})
public class NiseeAnswer implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NiseeAnswerPK niseeAnswerPK;
    @Basic(optional = false)
    @Column(name = "image_ANSWER")
    private String imageANSWER;
    @JoinColumn(name = "image_IMAGE_ID", referencedColumnName = "image_IMAGE_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private NiseeImage niseeImage;

    public NiseeAnswer() {
    }

    public NiseeAnswer(NiseeAnswerPK niseeAnswerPK) {
        this.niseeAnswerPK = niseeAnswerPK;
    }

    public NiseeAnswer(NiseeAnswerPK niseeAnswerPK, String imageANSWER) {
        this.niseeAnswerPK = niseeAnswerPK;
        this.imageANSWER = imageANSWER;
    }

    public NiseeAnswer(int imageIMAGEID, String imageTYPE) {
        this.niseeAnswerPK = new NiseeAnswerPK(imageIMAGEID, imageTYPE);
    }

    public NiseeAnswerPK getNiseeAnswerPK() {
        return niseeAnswerPK;
    }

    public void setNiseeAnswerPK(NiseeAnswerPK niseeAnswerPK) {
        this.niseeAnswerPK = niseeAnswerPK;
    }

    public String getImageANSWER() {
        return imageANSWER;
    }

    public void setImageANSWER(String imageANSWER) {
        this.imageANSWER = imageANSWER;
    }

    public NiseeImage getNiseeImage() {
        return niseeImage;
    }

    public void setNiseeImage(NiseeImage niseeImage) {
        this.niseeImage = niseeImage;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (niseeAnswerPK != null ? niseeAnswerPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeAnswer)) {
            return false;
        }
        NiseeAnswer other = (NiseeAnswer) object;
        if ((this.niseeAnswerPK == null && other.niseeAnswerPK != null) || (this.niseeAnswerPK != null && !this.niseeAnswerPK.equals(other.niseeAnswerPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeAnswer[niseeAnswerPK=" + niseeAnswerPK + "]";
    }

}
