/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Crespo
 */
@Embeddable
public class NiseeTestSinglePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "image_IMAGE_ID")
    private int imageIMAGEID;
    @Basic(optional = false)
    @Column(name = "test_GROUP_ID")
    private int testGROUPID;

    public NiseeTestSinglePK() {
    }

    public NiseeTestSinglePK(int imageIMAGEID, int testGROUPID) {
        this.imageIMAGEID = imageIMAGEID;
        this.testGROUPID = testGROUPID;
    }

    public int getImageIMAGEID() {
        return imageIMAGEID;
    }

    public void setImageIMAGEID(int imageIMAGEID) {
        this.imageIMAGEID = imageIMAGEID;
    }

    public int getTestGROUPID() {
        return testGROUPID;
    }

    public void setTestGROUPID(int testGROUPID) {
        this.testGROUPID = testGROUPID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) imageIMAGEID;
        hash += (int) testGROUPID;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeTestSinglePK)) {
            return false;
        }
        NiseeTestSinglePK other = (NiseeTestSinglePK) object;
        if (this.imageIMAGEID != other.imageIMAGEID) {
            return false;
        }
        if (this.testGROUPID != other.testGROUPID) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeTestSinglePK[imageIMAGEID=" + imageIMAGEID + ", testGROUPID=" + testGROUPID + "]";
    }

}
