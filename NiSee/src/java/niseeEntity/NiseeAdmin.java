/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package niseeEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Crespo
 */
@Entity
@Table(name = "nisee_admin")
@NamedQueries({
    @NamedQuery(name = "NiseeAdmin.findAll", query = "SELECT n FROM NiseeAdmin n"),
    @NamedQuery(name = "NiseeAdmin.findByAdminFAMILYNAME", query = "SELECT n FROM NiseeAdmin n WHERE n.adminFAMILYNAME = :adminFAMILYNAME"),
    @NamedQuery(name = "NiseeAdmin.findByAdminFIRSTNAME", query = "SELECT n FROM NiseeAdmin n WHERE n.adminFIRSTNAME = :adminFIRSTNAME"),
    @NamedQuery(name = "NiseeAdmin.findByAdminPRIVILEDGE", query = "SELECT n FROM NiseeAdmin n WHERE n.adminPRIVILEDGE = :adminPRIVILEDGE"),
    @NamedQuery(name = "NiseeAdmin.findByAdminPASSWORD", query = "SELECT n FROM NiseeAdmin n WHERE n.adminPASSWORD = :adminPASSWORD"),
    @NamedQuery(name = "NiseeAdmin.findByAdminUSERNAME", query = "SELECT n FROM NiseeAdmin n WHERE n.adminUSERNAME = :adminUSERNAME"),
    @NamedQuery(name = "NiseeAdmin.findByAdminEMAIL", query = "SELECT n FROM NiseeAdmin n WHERE n.adminEMAIL = :adminEMAIL")})
public class NiseeAdmin implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "admin_FAMILY_NAME")
    private String adminFAMILYNAME;
    @Basic(optional = false)
    @Column(name = "admin_FIRST_NAME")
    private String adminFIRSTNAME;
    @Basic(optional = false)
    @Column(name = "admin_PRIVILEDGE")
    private int adminPRIVILEDGE;
    @Basic(optional = false)
    @Column(name = "admin_PASSWORD")
    private String adminPASSWORD;
    @Id
    @Basic(optional = false)
    @Column(name = "admin_USERNAME")
    private String adminUSERNAME;
    @Basic(optional = false)
    @Column(name = "admin_EMAIL")
    private String adminEMAIL;

    public NiseeAdmin() {
    }

    public NiseeAdmin(String adminUSERNAME) {
        this.adminUSERNAME = adminUSERNAME;
    }

    public NiseeAdmin(String adminUSERNAME, String adminFAMILYNAME, String adminFIRSTNAME, int adminPRIVILEDGE, String adminPASSWORD, String adminEMAIL) {
        this.adminUSERNAME = adminUSERNAME;
        this.adminFAMILYNAME = adminFAMILYNAME;
        this.adminFIRSTNAME = adminFIRSTNAME;
        this.adminPRIVILEDGE = adminPRIVILEDGE;
        this.adminPASSWORD = adminPASSWORD;
        this.adminEMAIL = adminEMAIL;
    }

    public String getAdminFAMILYNAME() {
        return adminFAMILYNAME;
    }

    public void setAdminFAMILYNAME(String adminFAMILYNAME) {
        this.adminFAMILYNAME = adminFAMILYNAME;
    }

    public String getAdminFIRSTNAME() {
        return adminFIRSTNAME;
    }

    public void setAdminFIRSTNAME(String adminFIRSTNAME) {
        this.adminFIRSTNAME = adminFIRSTNAME;
    }

    public int getAdminPRIVILEDGE() {
        return adminPRIVILEDGE;
    }

    public void setAdminPRIVILEDGE(int adminPRIVILEDGE) {
        this.adminPRIVILEDGE = adminPRIVILEDGE;
    }

    public String getAdminPASSWORD() {
        return adminPASSWORD;
    }

    public void setAdminPASSWORD(String adminPASSWORD) {
        this.adminPASSWORD = adminPASSWORD;
    }

    public String getAdminUSERNAME() {
        return adminUSERNAME;
    }

    public void setAdminUSERNAME(String adminUSERNAME) {
        this.adminUSERNAME = adminUSERNAME;
    }

    public String getAdminEMAIL() {
        return adminEMAIL;
    }

    public void setAdminEMAIL(String adminEMAIL) {
        this.adminEMAIL = adminEMAIL;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adminUSERNAME != null ? adminUSERNAME.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NiseeAdmin)) {
            return false;
        }
        NiseeAdmin other = (NiseeAdmin) object;
        if ((this.adminUSERNAME == null && other.adminUSERNAME != null) || (this.adminUSERNAME != null && !this.adminUSERNAME.equals(other.adminUSERNAME))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "niseeEntity.NiseeAdmin[adminUSERNAME=" + adminUSERNAME + "]";
    }

}
