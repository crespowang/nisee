/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import niseeDatabase.niseeDB;
import niseeDatabase.niseeTempAnswerTable;

/**
 *
 * @author Crespo
 */
public class keepAnswer extends HttpServlet {
   private static String WINDOWCLOSE = "windowclose.jsp";
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet keepAnswer</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet keepAnswer at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         int groupid = (Integer)request.getSession().getAttribute("groupid");
         System.out.println("Persistancing..." + groupid);

        boolean keep = ((String)request.getParameter("persistance")).equalsIgnoreCase("yes") ? true : false;
        if(keep){
             
            HashMap<Integer, String> id_answer = (HashMap<Integer, String>) request.getSession().getAttribute("map");
          
            niseeDB db = new niseeDB();
            db.keepAnswer(id_answer, groupid);
            db.close();
            
        }else{
            System.out.println("Discard");
            niseeDB db = new niseeDB();
            db.cleanGroup(groupid);
            db.close();

        }

        response.sendRedirect(WINDOWCLOSE);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
