package niseeImageGenerator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
/*import java.awt.Font;
import java.awt.Graphics2D;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import javax.imageio.ImageIO;*/

/**
 *
 * @author stussy
 */
public class niImage {

    private ArrayList object;
    private ArrayList background;

    public void Draw(String format) {
/*
        try {
            System.out.println("IS this called?");
            //get a blank image
            File f1 = new File("..\\blank.JPG");

            BufferedImage bufferedImage = ImageIO.read(f1);
            Graphics2D g2 = (Graphics2D) bufferedImage.createGraphics();

            //fill white circle
            g2.setColor(Color.WHITE);
            g2.fillArc(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), 0, 360);
            g2.setColor(Color.red);
            g2.setFont(new Font("Serif", Font.PLAIN, 15));
            g2.drawString("NiSee", 0, 15);

            g2.setColor(Color.BLACK);

            //draw object in relevant format
            File objectf = new File("..\\object.JPG");
            BufferedImage bufferedImageObject = ImageIO.read(objectf);
            Graphics2D g2_ = (Graphics2D) bufferedImageObject.createGraphics();
            //if (format.equals("Square")) {
            //niSquare x = new niSquare(0, 0, 309);
            object = niShape.drawShape(bufferedImageObject, g2_);//, g2);
            //} else if (format.equals("Circle")) {
            //}

            for (int i = 0; i < object.size() - 1; i++) {
                ((niSquare) object.elementAt(i)).setColour(Color.red);
                ((niSquare) object.elementAt(i)).drawSquare(g2);
            }

            //draw background
            //place 10 large circles
            int MAX = 15;
            //if (format.equals("Square")) {
            //background = new T[1000];//background = new niSquare[1000];//niSquare mySquares[] = new niSquare[1000];
            //}
            background = new Vector();
            int x, y, size, count, missed, whitespace, total;
            total = 0;
            size = 20;
            count = 0;
            missed = 0;
            Random gen = new Random();
            //x = 150;
            //y = 150;
            whitespace = 15;
            while (count < MAX && missed < 100) {
                //need a better way to get random coordinates
                x = Math.abs(gen.nextInt(bufferedImage.getWidth()));
                y = Math.abs(gen.nextInt(bufferedImage.getHeight()));

                niSquare mySquare = new niSquare(x, y, size);
                if (!mySquare.checkDrawSquare(whitespace, bufferedImage, g2)) {
                    missed++;
                } else {
                    missed = 0;
                    //add to array
                    background.addElement(mySquare);  // background[count++] = mySquare;
                    count++;
                    total++;
                }
            }

            System.out.println("Large squares:" + count);


            //place 100 medium circles
            MAX = 10000;
            size = 15;
            count = 0;
            missed = 0;
            whitespace = 1;
            while (count < MAX && missed < 9000) {
                //need a better way to get random coordinates
                x = Math.abs(gen.nextInt() % bufferedImage.getWidth());
                y = Math.abs(gen.nextInt() % bufferedImage.getHeight());

                niSquare mySquare = new niSquare(x, y, size);
                if (!mySquare.checkDrawSquare(whitespace, bufferedImage, g2)) {
                    missed++;
                } else {
                    missed = 0;
                    //add to array
                    background.addElement(mySquare); //background[count++] = mySquare;//mySquares[count] = mySquare;
                    count++;
                    total++;
                }
                //count++;
            }
            System.out.println("Medium squares:" + count);

            //place 1000 small circles
            MAX = 1000;
            size = 10;
            count = 0;
            missed = 0;
            whitespace = 1;
            while (count < MAX && missed < 9999) {
                //need a better way to get random coordinates
                x = Math.abs(gen.nextInt() % bufferedImage.getWidth());
                y = Math.abs(gen.nextInt() % bufferedImage.getHeight());

                niSquare mySquare = new niSquare(x, y, size);
                if (!mySquare.checkDrawSquare(whitespace, bufferedImage, g2)) {
                    missed++;
                } else {
                    missed = 0;
                    //add to array
                    background.addElement(mySquare); //background[count++] = mySquare;//mySquares[count] = mySquare;
                    count++;
                    total++;
                }
                //count++;
            }

            File tempf = new File("..\\temp.JPG");//System.currentTimeMillis()
            ImageIO.write(bufferedImage, "JPG", tempf);

            BufferedImage temp = new BufferedImage(309, 309, BufferedImage.TYPE_INT_RGB);
            Graphics2D g2__ = (Graphics2D) temp.createGraphics();

            for (int i = 0; i < 309; i++) {
                for (int j = 0; j < 309; j++) {
                    temp.setRGB(i, j, Color.white.getRGB());
                }
            }

            for (int i = 0; i < object.size(); i++) {
                ((niSquare) object.elementAt(i)).setColour(Color.red);
                ((niSquare) object.elementAt(i)).drawSquare(g2__);
            }

            System.out.println("Small squares:" + count);
            System.out.println("total:" + total);

           
            
            //File myNewPic = new File("..\\out.JPG");//System.currentTimeMillis()
            //ImageIO.write(temp, "JPG", myNewPic);
        } catch (IOException ex) {
            System.out.println("ERROR");
        }*/
    }

    public int Invert(BufferedImage tshape, Coordinates shape[]) {
        int count = 0;
        for (int i = 0; i < tshape.getWidth(); i++) {
            for (int j = 0; j < tshape.getHeight(); j++) {
                if (tshape.getRGB(i, j) == Color.black.getRGB()) {
                    shape[count++] = new Coordinates(i, j);
                }
            }
        }

        for (int i = 0; i < tshape.getWidth(); i++) {
            for (int j = 0; j < tshape.getHeight(); j++) {
                tshape.setRGB(i, j, Color.black.getRGB());
            }
        }

        for (int i = 0; i < count; i++) {
            tshape.setRGB(shape[i].x, shape[i].y, Color.white.getRGB());
        }
        return count;
    }
}
