<%-- 
    Document   : review
    Created on : 05/09/2010, 12:44:03 PM
    Author     : Crespo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
           <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
        <title>Test Review</title>
    </head>
    <body>
        <h2>${requestScope.testdate}</h2>
        <hr />
        <table id="reviewtable">
            <c:forEach var="single" items="${requestScope.tabs}" varStatus="i">
            <tr>
             
                <th> <img alt="" height="150" width="150"  src="getImage?id=${single.image_id}" /></th>
                <td>Your answer : ${single.user_ans}<br />
                    Standard answer : ${single.correct_ans}<br /><br/>
                     <c:choose>
		<c:when test="${single.user_ans == single.correct_ans}">
                    <img src="images/tick.png" />
		</c:when>
		<c:otherwise>
		      <img src="images/error.png" />
		    </c:otherwise>
		</c:choose>
                </td>
        
            </tr>
            </c:forEach>
            
        </table>
    </body>
</html>
