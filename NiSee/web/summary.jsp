<%-- 
    Document   : summary
    Created on : 04/09/2010, 3:20:59 PM
    Author     : Crespo
--%>


<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <script type="application/javascript" src="js/popup.js"></script>
           <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
        <title>Summary</title>
    </head>
    <body onunload="opener.location=('center.jsp')">
        <h1>You have finished all test images</h1>
        <form action="keepAnswer" method="POST" >
        <table id ="summarytable">
            <tr><td>Image</td><td>Your answer</td><td>Correct Answer</td></tr>
            <c:forEach var="tab" items="${requestScope.tabs}" >
                <tr>
                    <td><img alt="" height="150" width="150"  src="getImage?id=${tab.image_id}" /> </td>
                    <td>${tab.user_ans}</td>
                <td>${tab.correct_ans}</td>
                <td>
                <c:choose>
		<c:when test="${tab.user_ans ==tab.correct_ans}">
                    <img alt=""  src="images/tick.png" />
		</c:when>
		<c:otherwise>
                    <img alt=""  src="images/error.png" />
		    </c:otherwise>
		</c:choose>
                </td>
                </tr>
            </c:forEach>

              <tr><td colspan="3"><h2>Do you wish to store this result?</h2></td></tr>
              <td><input type="image" value="Yes" name="persistance" src="images/save.png" /></td>
              <td><input type="image" value="No" name="persistance" src="images/discard.png"/></td>
            
             </table>
             </form>
               
    </body>
</html>
