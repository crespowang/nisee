<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>NiSee</title>
        <script type="application/javascript" src="js/popup.js"></script>
        <link rel="stylesheet" href="niseecss.css" type="text/css" media="screen" />
    </head>
    <body>
       
       	<div id="wrap">
	<div id="title">
		<h1>NiSee</h1>
	</div>
	<div id="side">
		<ul>
                    <li><a class="item" href=""  onclick="javascript:popUp('profile.jsp')">Profile</a></li>
			
			<li><a class="item" href="setting">Setting</a></li>
			
			<li><a class="item" href="logout">Logout</a></li>
			
		</ul>
	</div>
	<div id="content">
		 <h1>Welcome ${sessionScope.user.userFIRSTNAME} ${sessionScope.user.userFAMILYNAME} </h1>
                 <h2>Your testing history</h2>
                 <table id="historytable">
                     <thead><tr><th>Test No</th><th>Date</th><th>Review</th></tr></thead>
                     <tbody>
                     <c:forEach var="group" items="${sessionScope.user.niseeTestGroupCollection}" varStatus="i">
                          <c:choose>
		<c:when test="${(i.count) % 2 == 0}">
		    <tr class="even">
		</c:when>
		<c:otherwise>
		    <tr class="odd">
		    </c:otherwise>
		</c:choose>
                 
                             <td>${group.testGROUP}</td>
                             <td>${group.testDATE}</td>
                             <td><a href="" onclick="javascript:popUp('review?groupid=${group.testGROUPID}')">Review</a></td>
                         </tr>
                     </c:forEach>
                     </tbody>
                 </table>
                 <table>
                     <tr>
                         <td>
                             <input onmouseout="HideContent('quicktip'); return true;" onmouseover="ShowContent('quicktip'); return true;"  type="image" value="Quick Start"  src="images/quickstart.png"   onclick="javascript:popUp('prepareImage?type=1')"  />
                         </td>
                         <td>
                     <input  onmouseout="HideContent('newtip'); return true;" onmouseover="ShowContent('newtip'); return true;"  type="image" value="New Test"  src="images/newtest.png"   onclick="javascript:popUp('prepareImage?type=2')"  />
                         </td>
                     </tr>
                     
                         
                        
                          <tr>
                          
                      
                               <td><div id="quicktip">Quickly start new test using stored images </div></td>
                               <td><div id="newtip">Start a new test with images generated right now</div></td>

                       
                          </tr>
                 </table>
	</div>
	<div id="bottom">
		<p><a href="mailto:uow321@googlegroups.com">NiSee Developing Group</a></p>
	</div>

        </div>
       
    </body>
</html>
